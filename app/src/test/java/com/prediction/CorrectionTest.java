package com.prediction;
import com.example.android.softkeyboard.Correction;

import junit.framework.TestCase;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertArrayEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doReturn;


public class CorrectionTest extends TestCase {

    DataSource mDictionary;

	char[][] letters = {
			{'q','w','e','r','t','z','u','i','o','p'},
			{'a','s','d','f','g','h','j','k','l','š'},
			{'y','x','c','v','b','n','m','č','ć','ž'},
			{' ','đ','\0','.',',','!','?',':','\0','\0'}
	};

	@Before
	public void setUp() throws Exception {
		super.setUp();
		MockitoAnnotations.initMocks(this);
		mDictionary = Mockito.mock(DataSource.class);
        doReturn(-1).when(mDictionary).getFrequency(anyString());
        doReturn(1).when(mDictionary).getFrequency("stan");
        doReturn(1).when(mDictionary).getFrequency("kuća");
        doReturn(1).when(mDictionary).getFrequency("majica");
        doReturn(1).when(mDictionary).getFrequency("majice");
        doReturn(1).when(mDictionary).getFrequency("stan");
        doReturn(1).when(mDictionary).getFrequency("mama");
        doReturn(1).when(mDictionary).getFrequency("mama");
        doReturn(1).when(mDictionary).getFrequency("query");
	}

	@Test
	public void test() {
		 Correction tester = new Correction(letters, mDictionary);
		 ArrayList<String> list1 = new ArrayList(); 
		 list1.add("stan");
		    // assert statements
		 assertArrayEquals(list1.toArray(),tester.correct("stsn", new SuggestedWords(5)).toArray());
		    
	}
	@Test
	public void test1() {
		Correction tester = new Correction(letters, mDictionary);
		 ArrayList<String> list1 = new ArrayList(); 
		 list1.add("kuća");
		    // assert statements
		 assertArrayEquals(list1.toArray(),tester.correct("iuća", new SuggestedWords(5)).toArray());
		    
	}
	@Test
	public void test2() {
		Correction tester = new Correction(letters, mDictionary);
		 ArrayList<String> list1 = new ArrayList(); 
		 list1.add("kuća");
		    // assert statements
		 assertArrayEquals(list1.toArray(),tester.correct("kula", new SuggestedWords(5)).toArray());
		    
	}
	@Test
	public void test3() {
		Correction tester = new Correction(letters, mDictionary);
		 ArrayList<String> list1 = new ArrayList();
		 list1.add("majice");
		 list1.add("majica");
		    // assert statements
		 assertArrayEquals(list1.toArray(),tester.correct("majics", new SuggestedWords(5)).toArray());
		    
	}
	@Test
	public void test4() {
		Correction tester = new Correction(letters, mDictionary);
		 ArrayList<String> list1 = new ArrayList(); 
		 list1.add("query");
		 
		    // assert statements
		 assertArrayEquals(list1.toArray(),tester.correct("wuery", new SuggestedWords(5)).toArray());
		    
	}
	@Test
	public void test5() {
		Correction tester = new Correction(letters, mDictionary);
		 ArrayList<String> list1 = new ArrayList(); 
		 list1.add("stan");
		 
		    // assert statements
		 assertArrayEquals(list1.toArray(),tester.correct("sttaann", new SuggestedWords(5)).toArray());
		    
	}
	@Test
	public void mileta1() {
		 CorrectionDist tester = new CorrectionDist();
		 ArrayList<String> list1 = new ArrayList(); 
		 list1.add("stan");
		
		 tester.setWords(new Unigram[]{new Unigram("dan", 1), new Unigram("kran",1), new Unigram("stan", 2) });
		    // assert statements
		 assertArrayEquals(list1.toArray(), tester.GiveDistPrediction("sten").toArray());
		    
	}
	@Test
	public void mileta2() {
		 CorrectionDist tester = new CorrectionDist();
		 ArrayList<String> list1 = new ArrayList(); 
		 list1.add("majica");
		 list1.add("majice");
		 list1.add("mama");

        tester.setWords(new Unigram[]{new Unigram("majica", 1), new Unigram("mama", 1), new Unigram("tata", 2)});
		    // assert statements
		 assertArrayEquals(list1.toArray(),tester.GiveDistPrediction("majic").toArray());
		    
	}
}
