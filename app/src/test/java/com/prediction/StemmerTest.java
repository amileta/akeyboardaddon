package com.prediction;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import static org.mockito.Mockito.when;


/**
 * Created by Antonio on 11.7.2015..
 */
public class StemmerTest extends TestCase {

    StemmerRulesProvider provider;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        MockitoAnnotations.initMocks(this);
        InputStream rulesInput = new FileInputStream(new File(getClass().getResource("/rules.txt").toURI()));
        InputStream transformationsInput = new FileInputStream(new File(getClass().getResource("/transformations.txt").toURI()));
        provider = Mockito.mock(StemmerRulesProvider.class);
        when(provider.getRulesInputStream()).thenReturn(rulesInput);
        when(provider.getTransformationsInputStream()).thenReturn(transformationsInput);
    }

    @Test
    public void testistakniSlogotvornoR() throws Exception{
        Stemmer stemmer = new Stemmer(provider);
        assertEquals(stemmer.istakniSlogotvornoR("mr"), "mR");
    }

    @Test
    public void testimaSamoglasnik() throws Exception {
        Stemmer stemmer = new Stemmer(provider);
        assertTrue(stemmer.imaSamoglasnik("banana"));
    }

    @Test
    public void testtransformiraj() {

    }

    @Test
    public void testkorjenuj() throws Exception{
        Stemmer stemmer = new Stemmer(provider);
        assertEquals(stemmer.korjenuj("fakulteti".toLowerCase()), "fakultet");
    }




}
