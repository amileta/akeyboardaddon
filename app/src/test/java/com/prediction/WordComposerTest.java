package com.prediction;

import junit.framework.TestCase;

/**
 * Created by Antonio on 17.5.2015..
 */
public class WordComposerTest extends TestCase {

    public void testAdd() throws Exception {
        WordComposer wc = new WordComposer();
        wc.add('A', new int[]{'S', 'Q'});
        assertEquals(wc.getCodesAt(0)[0], 'S');
        assertEquals(wc.getCodesAt(0)[1], 'Q');
        assertEquals(wc.getTypedWord().toString(), "A");
    }

    public void testDeleteLast() throws Exception {
        WordComposer wc = new WordComposer();
        wc.add('A', new int[]{'S','Q'});
        wc.deleteLast();
        assertNull(wc.getTypedWord());
    }
}