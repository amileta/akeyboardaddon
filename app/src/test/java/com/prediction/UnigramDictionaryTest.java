package com.prediction;

import junit.framework.TestCase;

/**
 * Created by Antonio on 19.5.2015..
 */


public class UnigramDictionaryTest extends TestCase {
  /*
  TODO FIX THESE TESTS!
    @Mock
    Context context;
    @Mock
    ProximityInfo pi;
    //the file for initialization
    File file = null;
    //the locale (Mockito can't mock this!)
    Locale locale = null;
    WordComposer wc;
    SoftKeyboard ime;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        MockitoAnnotations.initMocks(this);
        file = new File(getClass().getResource("/testdict.txt").toURI());
        locale = new Locale("hr");
        wc = Mockito.mock(WordComposer.class);
        when(wc.isAutoCapitalized()).thenReturn(false);
        ime = Mockito.mock(SoftKeyboard.class);
        when(ime.getCurrentWord()).thenReturn(wc);
    }
    
    public void testIsValidWord() throws Exception {
        UnigramDictionary ug = new UnigramDictionary(context, ime, Dictionary.DICT_TYPE_UNIGRAM );
        //rijec u rijecniku bez frekvencije
        assertFalse(ug.isValidWord("Tes"));
        //nepostojeca rijec
        assertFalse(ug.isValidWord("Basdeaq"));
    }

    public void testAddWord() throws Exception {
        UnigramDictionary ug = new UnigramDictionary(context, ime, Dictionary.DICT_TYPE_UNIGRAM );
        ug.addWord("Test1", 1);
        assertTrue(ug.isValidWord("Test1"));
    }

    public void testGetWords() throws Exception {
        UnigramDictionary ug = new UnigramDictionary(context, ime, Dictionary.DICT_TYPE_UNIGRAM );
        WordComposer wc = new WordComposer();
        class TestDict implements Dictionary.WordCallback  {
            String[] words = new String[16];
            int index = 0;

            @Override
            public boolean addWord(char[] word, int wordOffset, int wordLength, int score, int dicTypeId, Dictionary.DataType dataType) {
                if (index < 16) {
                    words[index] = new String(word, wordOffset, wordLength);
                    index++;
                    return true;
                }
                return false;
            }

            public int getWordsCount(){
                return index;
            }

            public void printWords(){
                for (int i=0; i<index; i++){
                    System.out.println(words[i]);
                }
            }

        }
        TestDict td = new TestDict();
        wc.add('B',new int[]{'B'});
        wc.add('a',new int[]{'a'});
        wc.add('r',new int[]{'r'});
        ug.getWords(wc,td, pi);
        td.printWords();
        assertEquals(td.getWordsCount(),2);
    }
    */
}