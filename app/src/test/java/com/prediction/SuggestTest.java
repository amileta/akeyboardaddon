package com.prediction;

import android.content.Context;

import com.example.android.softkeyboard.SoftKeyboard;


import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TemporaryFolder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.File;
import java.util.Locale;

import static org.mockito.Mockito.when;


/**
 * Created by Antonio on 17.5.2015..
 */
public class SuggestTest extends TestCase {

    @Rule
    public TemporaryFolder folder;
    @Mock
    Context context;
    @Mock
    ProximityInfo info;
    //the file for initialization
    File file = null;
    //the locale (Mockito can't mock this!)
    Locale locale = null;
    WordComposer wc;
    SoftKeyboard ime;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        MockitoAnnotations.initMocks(this);
        file = new File(getClass().getResource("/testdict.txt").toURI());
        locale = new Locale("hr");
        wc = Mockito.mock(WordComposer.class);
        when(wc.isAutoCapitalized()).thenReturn(false);
        ime = Mockito.mock(SoftKeyboard.class);
        when(ime.getCurrentWord()).thenReturn(wc);
    }

    @After
    public void tearDown() throws Exception {

    }
  /*
  TODO FIX THESE TESTS!
    @Test
    public void testGetUnigramDictionaries() throws Exception {
        Suggest sg = new Suggest(context, ime, file, locale, file);
        assertNotNull(sg.getUnigramDictionaries());
    }
    @Test
    public void testGetSuggestions() throws Exception {
        WordComposer wc = new WordComposer();
        wc.add('T', new int[]{'R','Z'});
        wc.add('E', new int[]{'W','R'});
        wc.add('S', new int[]{'A','D'});
        wc.add('T', new int[]{'R', 'Z'});
        Suggest sg = new Suggest(context, ime, file, locale, file);
        List<String> list = sg.getSuggestions(wc,"Test1",info);
        for (String word : list){
            Log.d("TEST",word.toString());
        }

        assertNotNull(list);
    }
    @Test
    public void testAddWord() throws Exception {
        Suggest sg = new Suggest(context, ime, file, locale,file );
        char[] word = new char[]{'w','o','r','d'};
        assertTrue(sg.addWord(word,0,4,2,Dictionary.DICT_TYPE_UNIGRAM, Dictionary.DataType.UNIGRAM));
    }
*/
}