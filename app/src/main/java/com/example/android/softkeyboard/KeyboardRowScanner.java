package com.example.android.softkeyboard;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.inputmethodservice.Keyboard;
import android.util.Log;


/**
 * Created by Antonio on 17.12.2015..
 */
public class KeyboardRowScanner extends KeyboardLinearScanner  {

    public boolean isInRow = false;

    //todo use an interface instead of LatinKeyboard class for greater future flexibility
    public KeyboardRowScanner(LatinKeyboard keyboard, Context context) {
        super(keyboard, context);
    }

    @Override
    public void draw(Canvas canvas){
        drawByRows(canvas);
    }

    @Override
    public void scan(){
        super.scan();
        if (isInRow){
            if (mKeyboard.isValidKey(mCurrentRow, mCurrentColumn)){
                //increment column counter
                if (direction == DIRECTION_RIGHT) {
                    mCurrentColumn = (mCurrentColumn + 1) % mKeyboard.getColumnsNum(mCurrentRow);
                } else {
                    mCurrentColumn = (mCurrentColumn - 1) % mKeyboard.getColumnsNum(mCurrentRow);
                    if (mCurrentColumn == INDEX_START)
                        mCurrentColumn = mKeyboard.getColumnsNum(mCurrentRow) - 1;
                }
            }
        } else {
            //increment row counter
            if (direction == DIRECTION_RIGHT) {
                mCurrentRow = (mCurrentRow + 1) % mKeyboard.getRowsNum();
            } else {
                mCurrentRow = (mCurrentRow - 1) % mKeyboard.getRowsNum();
                if (mCurrentRow == INDEX_START)
                    mCurrentRow = mKeyboard.getRowsNum() - 1;
            }
        }
    }


    @Override
    public boolean nextLevel(){
        //keypress happened
        if (isInRow) {
            isInRow = false;
            if (mResetAfterSelect) {
                mCurrentRow = 0;
            }
            mCurrentColumn = 0;
            direction = DIRECTION_RIGHT;
            return true;
        }
        //reset direction
        direction = DIRECTION_RIGHT;
        //enter column
        isInRow = true;

        if (mResetAfterSelect) {
            mCurrentColumn = 0;
        }
        return false;
    }

    @Override
    public void resetCounters(){
        super.resetCounters();
        isInRow = false;
    }

    private void drawByRows(Canvas canvas){

        if (isInRow){
            if (mKeyboard.isValidKey(mCurrentRow,mCurrentColumn)){
                //get the current key
                Keyboard.Key key = mKeyboard.getKey(mCurrentRow,mCurrentColumn);
                mCurrentKey = key;
                //Log.d("KB", "Current key:" + LatinKeyboardView.currentKey);
                //set the mHighlight bounds
                mHighlight.setBounds(key.x, key.y, key.x + key.width, key.y + key.height);
                mHighlight.draw(canvas);
            }
            drawBorderHighlightInner(canvas);
        }
        else {
            //mHighlight all keys in the current row
            for (int i=0; i < mKeyboard.getColumnsNum(mCurrentRow); i++){
                //get the current key
                if (mKeyboard.isValidKey(mCurrentRow,i)) {
                    Keyboard.Key key = mKeyboard.getKey(mCurrentRow,i);
                    //set the mHighlight bounds
                    mHighlight.setBounds(key.x, key.y, key.x + key.width, key.y + key.height);
                    mHighlight.draw(canvas);
                }
            }
            drawBorderHighlight(canvas);
        }

    }

    @Override
    protected void drawBorderHighlightInner(Canvas canvas) {
        //get the first and last key in row
        Keyboard.Key first = mKeyboard.getKey(mCurrentRow,0);
        Keyboard.Key last = mKeyboard.getKey(mCurrentRow, mKeyboard.getColumnsNum(mCurrentRow) - 1);
        //highlight keyboard bounds
        Rect rect = new Rect(
                first.x, first.y,
                last.x+last.width,
                last.y+last.height);
        canvas.drawRect(rect, mBorderZonePaint);
    }
}
