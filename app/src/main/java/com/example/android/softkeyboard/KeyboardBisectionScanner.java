package com.example.android.softkeyboard;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.inputmethodservice.Keyboard;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Kruno on 12.12.2015..
 */
public class KeyboardBisectionScanner extends KeyboardScanner {

    private final int SIDE_NONE = -1;
    private final int SIDE_LEFT = 0;
    private final int SIDE_RIGHT = 1;
    private final int SIDE_UP = 2;
    private final int SIDE_DOWN = 3;
    private int SIDE_START;

    private int mCurrentRound; //current number of round in bisection
    private int mCurrentRowNum; //current number of row for that round
    private ArrayList<Integer> mCurrentColumnsNum; //column count of previous round
    private ArrayList<Integer> upperLeftRoundKey;
    private ArrayList<Integer> mStartColumnsNum; //column count at start
    private int mCurrentSide; //to know on which side to draw true -> draw left side; false -> draw right side
    private boolean mIsNextLevel;
    private boolean isEvenLayout;
    private int mCurrentStep;

    private Drawable mDrawable;

    public KeyboardBisectionScanner(LatinKeyboard keyboard, Context context, int SIDE_START) {
        super(keyboard, context);
        mDrawable = context.getResources().getDrawable(R.drawable.dark_green_tint);
        this.SIDE_START = SIDE_START;
        mCurrentRowNum = mKeyboard.getRowsNum();
        mCurrentRound = 1;
        if (upperLeftRoundKey != null){
            upperLeftRoundKey.set(0,0);
            upperLeftRoundKey.set(1,0);
        } else {
            upperLeftRoundKey = new ArrayList<Integer>();
            upperLeftRoundKey.add(0);
            upperLeftRoundKey.add(0);
        }
        mCurrentColumnsNum = (ArrayList<Integer>)mKeyboard.getColumnsNum().clone();
        mStartColumnsNum = (ArrayList<Integer>)mKeyboard.getColumnsNum().clone();
        mIsNextLevel = true;
        mCurrentStep = 0;
        isEvenLayout = true;
    }

    @Override
    public void scan() {
        super.scan();
        if (mCurrentRowNum == 1 && mCurrentColumnsNum.get(upperLeftRoundKey.get(0)) <= 3) {
            mIsNextLevel = false;
            int j = upperLeftRoundKey.get(1) + mCurrentStep;
            if (mKeyboard.isValidKey(upperLeftRoundKey.get(0),j)) {
                mCurrentStep = (mCurrentStep + 1) % mCurrentColumnsNum.get(upperLeftRoundKey.get(0));
            }
        } else if (mCurrentColumnsNum.get(upperLeftRoundKey.get(0)) == 1 && mCurrentRowNum <= 3){
            mIsNextLevel = false;
            int i = mCurrentRowNum + mCurrentStep;
            if (mKeyboard.isValidKey(i,mCurrentColumnsNum.get(0))){
                mCurrentStep = (mCurrentStep + 1) % mCurrentRowNum;
            }
        } else {
            if (mCurrentSide == SIDE_LEFT)
                mCurrentSide = SIDE_RIGHT;
            else if (mCurrentSide == SIDE_RIGHT)
                mCurrentSide = SIDE_LEFT;
            else if (mCurrentSide == SIDE_UP)
                mCurrentSide = SIDE_DOWN;
            else if (mCurrentSide == SIDE_DOWN)
                mCurrentSide = SIDE_UP;
            else
                mCurrentSide = SIDE_START; //bisection on start
        }
    }

    @Override
    public void draw(Canvas canvas) {
        bisection(canvas);
    }

    @Override
    public boolean nextLevel(){
        if(!mIsNextLevel){
            mIsNextLevel = true;
            return true;
        }
        else {
            if (((mCurrentRound & 1) == 0 && SIDE_START == SIDE_LEFT) || ((mCurrentRound & 1) != 0 && SIDE_START == SIDE_UP)){ //even number and bisection by columns meaning j of upperLeftRoundKey[i][j] does change
                int isRowDivisionOdd = 0;
                if ((mCurrentRowNum % 2) != 0){
                    isRowDivisionOdd = 1;
                    if (mCurrentSide == SIDE_UP)
                        mCurrentRowNum = mCurrentRowNum/2 + 1;
                    else if (mCurrentSide == SIDE_DOWN)
                        mCurrentRowNum = mCurrentRowNum/2;
                } else
                    mCurrentRowNum/=2;
                if (mCurrentSide == SIDE_DOWN)
                    upperLeftRoundKey.set(0,upperLeftRoundKey.get(0) + mCurrentRowNum + isRowDivisionOdd);
                mCurrentSide = SIDE_LEFT; //set current side for next level of bisection
            } else if (((mCurrentRound & 1) != 0 && SIDE_START == SIDE_LEFT) || ((mCurrentRound & 1) == 0 && SIDE_START == SIDE_UP)){
                ArrayList<Boolean> isColDivisionOdd = new ArrayList<Boolean>();
                for (int i = 0; i<mCurrentColumnsNum.size(); i++)
                    if ((mCurrentColumnsNum.get(i) % 2) != 0) { //half isn't an even number
                        isColDivisionOdd.add(i, true);
                        if (mCurrentSide == SIDE_LEFT)
                            mCurrentColumnsNum.set(i, (mCurrentColumnsNum.get(i) / 2) + 1);
                        else if (mCurrentSide == SIDE_RIGHT)
                            mCurrentColumnsNum.set(i, mCurrentColumnsNum.get(i) / 2);
                    } else {
                        isColDivisionOdd.add(i, false);
                        mCurrentColumnsNum.set(i, mCurrentColumnsNum.get(i) / 2);
                    }
                if (mCurrentSide == SIDE_RIGHT && isColDivisionOdd.get(upperLeftRoundKey.get(0)))
                    upperLeftRoundKey.set(1,upperLeftRoundKey.get(1) + mCurrentColumnsNum.get(upperLeftRoundKey.get(0)) + 1);
                else if (mCurrentSide == SIDE_RIGHT && !isColDivisionOdd.get(upperLeftRoundKey.get(0)))
                    upperLeftRoundKey.set(1,upperLeftRoundKey.get(1) + mCurrentColumnsNum.get(upperLeftRoundKey.get(0)));
                mCurrentSide = SIDE_UP; //set current side for next level of bisection
                Log.d("layout", "isColDivisionOdd" + isColDivisionOdd);
            }
            mCurrentRound++;
            Log.d("layout", "upperleft i:" + upperLeftRoundKey.get(0) + " j:" + upperLeftRoundKey.get(1));
            Log.d("layout", "currentcolumnnum " + mCurrentColumnsNum.get(upperLeftRoundKey.get(0)));
            return false;
        }
    }

    @Override
    public void resetCounters() {
        mCurrentRound = 1;
        mCurrentSide = SIDE_NONE;
        upperLeftRoundKey.set(0,0);
        upperLeftRoundKey.set(1,0);
        mCurrentRowNum = mKeyboard.getRowsNum();
        mCurrentColumnsNum = (ArrayList<Integer>)mKeyboard.getColumnsNum().clone();
        mIsNextLevel = true;
        mCurrentStep = 0;
    }

    private void bisection(Canvas canvas) {

        if (mCurrentSide == SIDE_NONE)
            mCurrentSide = SIDE_START;

        if (mCurrentRowNum == 1 && mCurrentColumnsNum.get(upperLeftRoundKey.get(0)) <= 3){
            mIsNextLevel = false;
            int i = upperLeftRoundKey.get(0);
            int j = upperLeftRoundKey.get(1) + mCurrentStep;
            if (mKeyboard.isValidKey(i,j)){
                Keyboard.Key key = mKeyboard.getKey(i, j);
                mCurrentKey = key;
                drawKey(i, j, canvas);
            }
            drawBorderHighlight(canvas);
        } else if (mCurrentColumnsNum.get(upperLeftRoundKey.get(0)) == 1 && mCurrentRowNum <= 3){
            mIsNextLevel = false;
            int i = mCurrentRowNum + mCurrentStep;
            int j = mCurrentColumnsNum.get(upperLeftRoundKey.get(1));
            if (mKeyboard.isValidKey(i,j)){
                Keyboard.Key key = mKeyboard.getKey(i,j);
                mCurrentKey = key;
                drawKey(i, j, canvas);
            }
        } else {
            for (int i = upperLeftRoundKey.get(0); i < upperLeftRoundKey.get(0) + mCurrentRowNum; i++) {
                for (int j = upperLeftRoundKey.get(1); j < upperLeftRoundKey.get(1) + mCurrentColumnsNum.get(i); j++) {
                    int isColDivisionOdd = 0;
                    int isRowDivisionOdd = 0;
                    if ((mCurrentColumnsNum.get(i) % 2) != 0)
                        isColDivisionOdd = 1;
                    if ((mCurrentRowNum % 2) != 0)
                        isRowDivisionOdd = 1;

                    if (mCurrentSide == SIDE_LEFT) { //draw left
                        if (j < upperLeftRoundKey.get(1) + (mCurrentColumnsNum.get(i) / 2) + isColDivisionOdd) {
                            Log.d("valid", "i:" + i + " j:" + j);
                            if (mKeyboard.isValidKey(i, j))
                                drawKey(i, j, canvas);
                        }
                    } else if (mCurrentSide == SIDE_RIGHT) { //draw right
                        if (j >= upperLeftRoundKey.get(1) + (mCurrentColumnsNum.get(i) / 2) + isColDivisionOdd) {
                            Log.d("valid", "i:" + i + " j:" + j);
                            if (mKeyboard.isValidKey(i, j))
                                drawKey(i, j, canvas);
                        }
                    } else if (mCurrentSide == SIDE_UP) {
                        if (i < upperLeftRoundKey.get(0) + (mCurrentRowNum / 2) + isRowDivisionOdd) {
                            Log.d("valid", "i:" + i + " j:" + j);
                            if (mKeyboard.isValidKey(i, j))
                                drawKey(i, j, canvas);
                        }
                    } else if (mCurrentSide == SIDE_DOWN) {
                        if (i >= upperLeftRoundKey.get(0) + (mCurrentRowNum / 2) + isRowDivisionOdd) {
                            Log.d("valid", "i:" + i + " j:" + j);
                            if (mKeyboard.isValidKey(i, j))
                                drawKey(i, j, canvas);
                        }
                    }
                }
            }
            for (int i = 0; i < mStartColumnsNum.size(); i++)
                 if (mStartColumnsNum.get(i) != 10)
                     isEvenLayout = false;
            if (mKeyboard.getRowsNum() == 4 && isEvenLayout)
                drawBorderHighlight(canvas);
        }
    }


    protected void drawBorderHighlight(Canvas canvas){
        int start_row = upperLeftRoundKey.get(0);
        int start_col = upperLeftRoundKey.get(1);
        int end_row = start_row + mCurrentRowNum - 1;
        Keyboard.Key start = mKeyboard.getKey(start_row, start_col);
        Keyboard.Key end = mKeyboard.getKey(end_row,start_col + mCurrentColumnsNum.get(end_row)-1);
        Rect rect = new Rect(start.x, start.y, end.x + end.width-1, end.y + end.height);
        canvas.drawRect(rect, mBorderZonePaint);
    }

    protected void drawBorderHighlightInner(Canvas canvas){

    }

    private void drawKey(int i, int j, Canvas canvas){
        Keyboard.Key key = mKeyboard.getKey(i,j);
        mDrawable.setBounds(key.x, key.y, key.x + key.width, key.y + key.height);
        mDrawable.draw(canvas);
    }
}
