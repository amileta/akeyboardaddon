package com.example.android.softkeyboard;

import android.content.SharedPreferences;
import android.os.Handler;

/**
 * Created by Antonio on 26.11.2015..
 */
public class CandidateScanner {

    public static final int INDEX_NONE = -1;

    private boolean isActive;
    private CandidateView mCandidateView;

    private int letter_speed;

    //the index of currently higlighted suggestion
    private int current_index;

    private SharedPreferences mSharedPreferences;

    public CandidateScanner(CandidateView candidateView, SharedPreferences preferences) {
        if (candidateView == null){
            throw new IllegalArgumentException("Passed candidateView is null!");
        }
        if (preferences == null){
            throw new IllegalArgumentException("Passed preferences are null!");
        }
        this.mCandidateView = candidateView;
        mSharedPreferences = preferences;
        reset();
    }

    public void setActive(boolean active) {
        viewHandler.removeCallbacks(updateView);
        if (active){
            viewHandler.post(updateView);
        }
        this.isActive = active;
    }

    public boolean getActive() {
        return this.isActive;
    }

    public void setCurrentIndex(int index){
        current_index = index;
    }

    public int getCurrentIndex() {
        return current_index;
    }

    public void reset(){
        /** Reset the scanner to inital state */
        isActive = false;
        current_index = INDEX_NONE;
        letter_speed = Integer.parseInt(mSharedPreferences.getString("lettspeed", "1000"));
    }

    private Handler viewHandler = new Handler();

    public Runnable updateView = new Runnable() {
        @Override
        public void run() {
            if (isActive) {
                int size = mCandidateView.getVisibleSuggestionsSize();
                if (size != 0)
                    current_index = (current_index + 1) % size;
                else
                    current_index = INDEX_NONE;
                mCandidateView.invalidate();
                mCandidateView.setSelectedIndex(current_index);
                viewHandler.postDelayed(updateView, letter_speed);
            }
        }
    };
}
