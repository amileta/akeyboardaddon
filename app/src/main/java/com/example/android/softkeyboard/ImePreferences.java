/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.softkeyboard;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.util.Log;
import android.view.View;

import com.android.inputmethodcommon.InputMethodSettingsFragment;
import com.prediction.FileChooser;
import com.prediction.FileHelper;
import com.prediction.MySQLiteHelper;

import java.io.File;
import java.io.IOException;

/**
 * Displays the IME preferences inside the input method setting.
 */
public class ImePreferences extends PreferenceActivity {

    protected MySQLiteHelper dbHelper;

    private ProgressDialog dialog;

    @Override
    public Intent getIntent() {
        final Intent modIntent = new Intent(super.getIntent());
        modIntent.putExtra(EXTRA_SHOW_FRAGMENT, Settings.class.getName());
        modIntent.putExtra(EXTRA_NO_HEADERS, true);
        return modIntent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // We overwrite the title of the activity, as the default one is "Voice Search".
        setTitle(R.string.settings_name);

        //todo find out how to get Context when this is refactored into a PreferenceFragment
        dbHelper = MySQLiteHelper.getInstance(this);
    }

    @Override
    protected boolean isValidFragment(final String fragmentName) {
        return Settings.class.getName().equals(fragmentName);
    }

    public static class Settings extends InputMethodSettingsFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setInputMethodSettingsCategoryTitle(R.string.language_selection_title);
            setSubtypeEnablerTitle(R.string.select_language);

            // Load the preferences from an XML resource
            addPreferencesFromResource(R.xml.ime_preferences);
        }
    }

    public void resetPredictionDb(View view){
        dialog = new ProgressDialog(this);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage("Reset in progress. Please wait...");
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        new ResetDBTask().execute(dbHelper.getWritableDatabase());
    }

    public void importPredictionDb(View view){
        new FileChooser(this).setFileListener(new FileChooser.FileSelectedListener() {
            @Override
            public void fileSelected(final File file) {;
                // copy database to data directory
                try {
                    // open a new file in data directory
                    String name = file.getName();
                    String[] parts = name.split("\\.");
                    if (parts.length <= 1 || !(parts[parts.length-1].equals("db")))
                        throw new IOException("The selected file is not a SQLITE database!");
                    File new_file = new File(MySQLiteHelper.DB_DIR + name);
                    FileHelper.copyFile(file, new_file);
                    // make helper use the new database
                    MySQLiteHelper.switchDatabase(new_file.getName());
                } catch (IOException e) {
                    Log.e("DATABASE","Error importing new dictionary database!");
                    AlertDialog.Builder builder = new AlertDialog.Builder(ImePreferences.this);
                    builder.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    builder.setMessage("Error importing dictionary database! " + e.getMessage());
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            }
        }).showDialog();
    }

    private class ResetDBTask extends AsyncTask<SQLiteDatabase, Integer, Boolean>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.show();
        }

        @Override
        protected Boolean doInBackground(SQLiteDatabase... sqLiteDatabases) {
            if (sqLiteDatabases.length > 0) {
                SQLiteDatabase db = sqLiteDatabases[0];
                db.close();
                dbHelper.deleteDatabase();
                try {
                    dbHelper.copyDatabase();
                } catch (IOException e) {
                    return false;
                }
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            dialog.dismiss();
            //todo display alert dialog
        }
    }
}
