/*
 * Copyright (C) 2008-2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.softkeyboard;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.Paint;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.Keyboard.Key;
import android.inputmethodservice.KeyboardView;
import android.media.AudioManager;
import android.os.Handler;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.v4.view.GestureDetectorCompat;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.inputmethod.InputMethodSubtype;


public class LatinKeyboardView extends KeyboardView {

    //FSM constants
    public static final int MODE_NORMAL = 0;
    public static final int MODE_SCAN_ROWS = 1;
    public static final int MODE_SCAN_COLUMNS = 2;
    public static final int MODE_BISECT_ROWS = 3;
    public static final int MODE_BISECT_COLUMNS = 4;
    public static final int MODE_SCAN_ROWS_NO_RESET = 5;
    public static final int MODE_SCAN_COLUMNS_NO_RESET = 6;

    //bisection start constants
    public static final int BISECT_ROWS_START = 2;
    public static final int BISECT_COLS_START = 0;

    public static int mCurrentMode = 0;

    static final int KEYCODE_OPTIONS = -100;
    // TODO: Move this into android.inputmethodservice.Keyboard
    //static final int KEYCODE_LANGUAGE_SWITCH = -101;
    static final int KEYCODE_SETTINGS = -101;
    static final int KEYCODE_CLOSE = -3;
    static final int KEYCODE_CHANGE_DIRECTION = -102;
    static final int KEYCODE_TOGGLE_PREDICTION = -103;
    static final int KEYCODE_RESET_SCAN = -104;

    private final float key_height = getResources().getDimension(R.dimen.key_height);
    //todo does this need to be static?
    public static int currentKey; //current key code
    boolean isTablet = getResources().getBoolean(R.bool.isTablet);
    private CandidateView mCandidateView;

    public KeyboardScanner mScanner;
    private Context mContext;
    private float height = 5*key_height; //height of keyboard
    private double radius; //radius for circle
    private SharedPreferences sharedPreferences;
    private boolean isVibrateOn;
    private boolean isSoundFeedbackOn;
    private boolean prefListChangeDirVisible;
    private int letter_speed;

    private double distanceFromCenter;

    private DisplayMetrics metrics = this.getResources().getDisplayMetrics();

    private Bitmap change_dir;
    private Bitmap tap_letter;
    private Bitmap backspace_button;
    private Bitmap enter_button;
    private Bitmap prediction_button;
    private Bitmap prediction_button_back;

    private Paint paint_circle;
    private Paint paint_lines;

    /* paints for buttons */
    private Paint paint_change_dir;
    private Paint paint_tap_letter;
    private Paint paint_enter_button;
    private Paint paint_backspace_button;
    private Paint paint_prediction_button;

    private ColorFilter highlightPositiveColorFilter = new LightingColorFilter(Color.YELLOW, Color.YELLOW);
    private ColorFilter highlightNegativeColorFilter = new LightingColorFilter(Color.RED, Color.RED);

    private boolean resetScannerPressed = false;

    private GestureDetectorCompat mDetector;

    private AudioManager am;

    public Handler viewHandler = new Handler();
    public Runnable updateView = new Runnable() {
        @Override
        public void run() {
            if (mCurrentMode != MODE_NORMAL) {
                if (mScanner != null) mScanner.scan();
                invalidate();
                viewHandler.postDelayed(updateView, letter_speed);
            }
        }
    };

    public LatinKeyboardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (isTablet){
            radius = 0.4*height;
        } else {
            radius = 0.25*height;
        }
        mContext = context;
        mDetector = new GestureDetectorCompat(this.mContext, new LongPressGestureListener());
        mDetector.setIsLongpressEnabled(true);
        initializeDrawables();
        am = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
    }

    public LatinKeyboardView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        if (isTablet){
            radius = 0.4*height;
        } else {
            radius = 0.25*height;
        }
        mContext = context;
        mDetector = new GestureDetectorCompat(this.mContext, new LongPressGestureListener());
        mDetector.setIsLongpressEnabled(true);
        initializeDrawables();
        am = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
    }

    private void initializeDrawables(){
        //postavi slike za overlay
        change_dir = BitmapFactory.decodeResource(getResources(), R.drawable.change_direction);
        tap_letter = BitmapFactory.decodeResource(getResources(), R.drawable.tap_letter);
        backspace_button = BitmapFactory.decodeResource(getResources(), R.drawable.backspace_button);
        enter_button = BitmapFactory.decodeResource(getResources(), R.drawable.enter_button);
        prediction_button = BitmapFactory.decodeResource(getResources(), R.drawable.prediction_button);
        prediction_button_back = BitmapFactory.decodeResource(getResources(), R.drawable.prediction_button_back);
        paint_circle = new Paint();
        paint_lines = new Paint();
        paint_change_dir = new Paint();
        paint_backspace_button = new Paint();
        paint_enter_button = new Paint();
        paint_tap_letter = new Paint(Color.YELLOW);
        paint_prediction_button = new Paint();
    }

    @Override
    protected boolean onLongPress(Key key) {
        if (key.codes[0] == Keyboard.KEYCODE_CANCEL) {
            getOnKeyboardActionListener().onKey(KEYCODE_OPTIONS, null);
            return true;
        } else {
            return super.onLongPress(key);
        }
    }

    @Override
    public void setKeyboard(Keyboard keyboard) {
        super.setKeyboard(keyboard);
        if (mScanner != null) {
            mScanner.setKeyboard((LatinKeyboard) keyboard);
        }
    }

    void setSubtypeOnSpaceKey(final InputMethodSubtype subtype) {
        final LatinKeyboard keyboard = (LatinKeyboard)getKeyboard();
        keyboard.setSpaceIcon(getResources().getDrawable(subtype.getIconResId()));
        invalidateAllKeys();
    }

    @Override
    public void setOnKeyboardActionListener(OnKeyboardActionListener listener) {
        super.setOnKeyboardActionListener(listener);
    }

    @Override
    public void onDraw(Canvas canvas) {
        if (!isScanningMode())
            super.onDraw(canvas);
        else {
            super.onDraw(canvas);
            int width = getWidth();
            int height = getHeight();
            
            if (prefListChangeDirVisible) {
                canvas.drawBitmap(change_dir, (float) ((width / 4) - change_dir.getWidth() / 2), (float) ((0.75 * height) - change_dir.getHeight() / 2), paint_change_dir);
            }
            if (SoftKeyboard.mSelectTE) {
                paint_tap_letter.setColorFilter(highlightPositiveColorFilter);
            } else if (resetScannerPressed) {
                paint_tap_letter.setColorFilter(highlightNegativeColorFilter);
                resetScannerPressed = false;
            }
            else {
                paint_tap_letter.setColorFilter(null);
            }
            canvas.drawBitmap(tap_letter, (float)(((width/2-radius)+radius) - tap_letter.getWidth()/2), (float)(((height/2-radius)+radius) - tap_letter.getHeight()/2), paint_tap_letter);
            canvas.drawBitmap(backspace_button, (float)((0.75*width) - backspace_button.getWidth()/2), (float)((height/4) - backspace_button.getHeight()/2), paint_backspace_button);
            canvas.drawBitmap(enter_button, (float)((0.75*width) - enter_button.getWidth()/2), (float)((0.75*height) - enter_button.getHeight()/2), paint_enter_button);
            if (!mCandidateView.isScannerActive())
                canvas.drawBitmap(prediction_button, (float)((width/4) - change_dir.getWidth()/2), (float)((height/4) - change_dir.getHeight()/2), paint_prediction_button);
            else
                canvas.drawBitmap(prediction_button_back, (float)((width/4) - change_dir.getWidth()/2), (float)((height/4) - change_dir.getHeight()/2), paint_prediction_button);

            //postavi crte za granice touch eventa
            paint_circle.setAlpha(75);
            paint_circle.setStyle(Paint.Style.STROKE);
            paint_circle.setStrokeWidth((key_height / 4));
            //krug u sredini
            canvas.drawCircle((width / 2), (height / 2), (float)radius, paint_circle);

            paint_lines.setStrokeWidth((key_height / 4));
            paint_lines.setAlpha(75);
            //lijeno sredina crta
            canvas.drawLine(0, (height / 2), ((width / 2) - (float)radius), (height / 2), paint_lines);
            //desno sredina crta
            canvas.drawLine(((width / 2) + (float)radius), (height / 2), width, (height / 2), paint_lines);
            //gore sredina crta
            canvas.drawLine((width / 2), 0, (width / 2), ((height / 2) - (float)radius), paint_lines);
            //dolje sredina crta
            canvas.drawLine((width / 2), ((height / 2) + (float)radius), (width / 2), height, paint_lines);

            //oznaci slovo koje je aktivno

            if (mCurrentMode!= MODE_NORMAL && !mCandidateView.isScannerActive() && mScanner != null) {
                mScanner.draw(canvas);
                if (mScanner.getCurrentKey() != LatinKeyboard.NO_KEY) {
                    currentKey = mScanner.getCurrentKey();
                }
            }
        }
    }

    @Override
    public void closing() { //kod zatvaranja tipkovnice ugasi handlera!
        Log.d("KB", "closing keyboard view! -> closing() called!");
        viewHandler.removeCallbacks(updateView);
        SoftKeyboard.mChangeDirTE = false;
        SoftKeyboard.mSelectTE = false;
        SoftKeyboard.mBackspaceTE = false;
        SoftKeyboard.mEnterTE = false;
        //reinit candidate view
        mCandidateView.reinit();
        //reset keyboard scanner
        if (mScanner != null) mScanner.resetCounters();
    }

    @Override
    public boolean onTouchEvent(MotionEvent me) {
        if (!isScanningMode())
            return super.onTouchEvent(me);
        else {
            this.mDetector.onTouchEvent(me);
            super.onTouchEvent(me);

            float x = me.getX();
            float y = me.getY();
            float width = metrics.widthPixels;
            distanceFromCenter = Math.sqrt(Math.pow((double) ((width / 2) - x), 2) + Math.pow((double) ((height / 2) - y), 2));

            //Todo: refactor funkcija vezanih uz predikciju u callback
            switch (me.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    if (isVibrateOn){
                        Vibrator vibe = (Vibrator) mContext.getSystemService(Context.VIBRATOR_SERVICE);
                        vibe.vibrate(50);
                    }
                    if ((x < (width / 2) && y > (height / 2)) && distanceFromCenter > radius) { //dolje lijevi TE
                        lowerLeftFunction();
                    } else if ((x < (width / 2) && y < (height / 2)) && distanceFromCenter > radius) { //gore lijevi TE
                        upperLeftFunction();
                        return true;
                    } else if (distanceFromCenter < radius) { //srednji TE
                        middleFunction();
                        if (mCandidateView != null && mCandidateView.isScannerActive()) {
                            return true;
                        }
                    } else if ((x > (width / 2) && y < (height / 2)) && distanceFromCenter > radius){ //gore desni TE
                        SoftKeyboard.mBackspaceTE = true;
                    } else if ((x > (width / 2) && y > (height / 2)) && distanceFromCenter > radius){ //dolje desni TE
                        SoftKeyboard.mEnterTE = true;
                    }
            }
            return true;
        }
    }

    public void lowerLeftFunction(){
        SoftKeyboard.mChangeDirTE = !(SoftKeyboard.mChangeDirTE);
        if (mScanner != null && prefListChangeDirVisible) mScanner.toggleDirection();
    }

    public void upperLeftFunction(){
        //todo check if prediction is activated
        if (mCandidateView != null && sharedPreferences.getBoolean("prediction_on_off", false)) {
            if (mCandidateView.toggleScannerActive()) {
                invalidate(); //redraw keyboard
                resetSelection();
            }
        }
    }

    public void middleFunction(){
        //ako je aktivna predikcija
        if (mCandidateView != null && mCandidateView.isScannerActive()) {
            mCandidateView.selectCurrentSuggestion();
            resetSelection();
            return;
        }

        if (mCurrentMode != MODE_NORMAL) {
            boolean touched = mScanner.nextLevel();
            if (touched) {
                SoftKeyboard.mSelectTE = true;
                if (mCurrentMode == MODE_BISECT_ROWS ||
                        mCurrentMode == MODE_BISECT_COLUMNS)
                    resetSelection();
                if (isSoundFeedbackOn){
                    am.playSoundEffect(AudioManager.FX_KEYPRESS_STANDARD, 1);
                }
            }
            invalidate();
        }
        viewHandler.removeCallbacks(updateView);
        viewHandler.postDelayed(updateView, letter_speed);
    }

    public boolean isScanningMode(){ //first it was meant for knowing if scanning mode is active, now it looks if scanning or bisection is active
        Keyboard current = getKeyboard();
        if (current == SoftKeyboard.mScanningKeyboard || current == SoftKeyboard.mSymbolsScanKeyboard) {
            setPreviewEnabled(false);
            return true;
        }
        setPreviewEnabled(true);
        return false;
    }

    public void toggleHandler(){
        if (isScanningMode()){
            viewHandler.removeCallbacks(updateView);
            viewHandler.postDelayed(updateView, letter_speed);
        } else {
            viewHandler.removeCallbacks(updateView);
        }
    }

    public void setPreferenceValue(){
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        String prefListValueofSpeed = sharedPreferences.getString("lettspeed", "no selection");
        prefListChangeDirVisible = sharedPreferences.getBoolean("change_dir", true);
        mCurrentMode = Integer.parseInt(sharedPreferences.getString("mode", Integer.toString(MODE_NORMAL)));
        KeyboardScanner.isSoundOn = sharedPreferences.getBoolean("sound_key", false);
        isVibrateOn = sharedPreferences.getBoolean("vibrate", false);
        isSoundFeedbackOn = sharedPreferences.getBoolean("sound_feedback", false);
        if (prefListValueofSpeed == "no selection")
            letter_speed = 1000;
        else
            letter_speed = Integer.parseInt(prefListValueofSpeed);
        SoftKeyboard.mode = mCurrentMode;
    }

    public void setCandidateView(CandidateView view){
        mCandidateView = view;
    }

    public void resetSelection(){
        if (mScanner != null) mScanner.resetCounters();
    }

    public void initializeScanner(){
        switch (mCurrentMode) {
            case (MODE_SCAN_ROWS):
                mScanner = new KeyboardRowScanner((LatinKeyboard) getKeyboard(), mContext);
                Log.d("KB", "Initialized row scanner!");
                break;
            case (MODE_SCAN_COLUMNS):
                mScanner = new KeyboardColumnScanner((LatinKeyboard) getKeyboard(), mContext);
                Log.d("KB", "Initialized column scanner!");
                break;
            case (MODE_BISECT_ROWS):
                mScanner = new KeyboardBisectionScanner((LatinKeyboard) getKeyboard(), mContext, BISECT_ROWS_START);
                break;
            case (MODE_BISECT_COLUMNS):
                mScanner = new KeyboardBisectionScanner((LatinKeyboard) getKeyboard(), mContext, BISECT_COLS_START);
                break;
            case (MODE_SCAN_ROWS_NO_RESET):
                mScanner = new KeyboardRowScanner((LatinKeyboard) getKeyboard(), mContext);
                mScanner.setResetAfterSelect(false);
                break;
            case (MODE_SCAN_COLUMNS_NO_RESET):
                mScanner = new KeyboardColumnScanner((LatinKeyboard) getKeyboard(), mContext);
                mScanner.setResetAfterSelect(false);
                break;
        }
    }
    public void resetScan(){
        if (isSoundFeedbackOn){
            am.playSoundEffect(AudioManager.FX_KEYPRESS_RETURN, 1);
        }
        resetScannerPressed = true;
        SoftKeyboard.mSelectTE = false; //odbaci pritisak tipke
        mScanner.resetCounters();
        viewHandler.removeCallbacks(updateView);
        viewHandler.postDelayed(updateView,letter_speed);
        invalidate();
    }

    class LongPressGestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onDown(MotionEvent event){
            return true;
        }

        @Override
        public void onLongPress(MotionEvent event){
            if (distanceFromCenter < radius) { //samo na srednji TE
                resetScan();
            }
        }



    }
}