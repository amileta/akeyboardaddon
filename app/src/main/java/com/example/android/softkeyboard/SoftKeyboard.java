/*
 * Copyright (C) 2008-2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.softkeyboard;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.Dialog;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.inputmethodservice.InputMethodService;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.text.InputType;
import android.text.method.MetaKeyKeyListener;
import android.util.Log;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.CompletionInfo;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputMethodManager;
import android.view.inputmethod.InputMethodSubtype;

import com.prediction.SuggestedWords;
import com.prediction.WordComposer;
import com.prediction.PredictionService;
import com.prediction.UnigramDataSource;


import org.xmlpull.v1.XmlPullParserException;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Example of writing an input method for a soft keyboard.  This code is
 * focused on simplicity over completeness, so it should in no way be considered
 * to be a complete soft keyboard implementation.  Its purpose is to provide
 * a basic example for how you would get started writing an input method, to
 * be fleshed out as appropriate.
 */
public class SoftKeyboard extends InputMethodService 
        implements KeyboardView.OnKeyboardActionListener, SharedPreferences.OnSharedPreferenceChangeListener {

    static final String KOMUNIKATOR_PACKAGE_NAME = "hr.riteh.akeyboard";
    
    /**
     * This boolean indicates the optional example code for performing
     * processing of hard keys in addition to regular text generation
     * from on-screen interaction.  It would be used for input methods that
     * perform language translations (such as converting text entered on 
     * a QWERTY keyboard to Chinese), but may not be used for input methods
     * that are primarily intended to be used for on-screen text entry.
     */
    static final boolean PROCESS_HARD_KEYS = true;
    static final int ENTER_KEY = 10;

    private InputMethodManager mInputMethodManager;

    public static LatinKeyboardView mInputView;
    private CandidateView mCandidateView;
    private CompletionInfo[] mCompletions;
    
    private WordComposer mComposing = new WordComposer();
    private boolean mPredictionOn;
    private boolean mCompletionOn;
    private int mLastDisplayWidth;
    private boolean mCapsLock;
    private long mLastShiftTime;
    private long mMetaState;
    public static boolean mSelectTE = false; //select elements in keyboard touch event
    public static boolean mChangeDirTE = false;  //change direction touch event
    public static boolean mBackspaceTE = false; //backspace touch event
    public static boolean mEnterTE = false; //enter touch event
    
    private LatinKeyboard mSymbolsKeyboard;
    private LatinKeyboard mSymbolsShiftedKeyboard;
    public static LatinKeyboard mQwertyKeyboard;
    public static LatinKeyboard mScanningKeyboard;
    public static LatinKeyboard mSymbolsScanKeyboard;
    
    private LatinKeyboard mCurKeyboard;

    public static int mode;

    private String mWordSeparators;

    private String mPreviousWord;
    private SuggestedWords mSuggestions = new SuggestedWords(10);

    private SharedPreferences prefs;

    private boolean mIsCorrectionOn;

    /// CORRECTION VARIABLES ///
    private UnigramDataSource unigrams;
    private Correction mCorrection;

    /// CLIENTSIDE SERVICE VARIABLES ///
    /** Messenger for communicating with service. */
    Messenger mService = null;
    /** Flag indicating whether we have called bind on the service. */
    boolean mIsBound;
    private ServiceConnection mConnection;

    private Messenger mMessenger;

    /**
     * Main initialization of the input method component.  Be sure to call
     * to super class.
     */
    @Override public void onCreate() {
        super.onCreate();
        mInputMethodManager = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
        mWordSeparators = getResources().getString(R.string.word_separators);

        /**
         * Target we publish for clients to send messages to IncomingHandler.
         */
        mMessenger = new Messenger(new IncomingHandler());

        /**
         * Class for interacting with the main interface of the service.
         */
        mConnection = new ServiceConnection() {
            public void onServiceConnected(ComponentName className,
                                           IBinder service) {
                // This is called when the connection with the service has been
                // established, giving us the service object we can use to
                // interact with the service.  We are communicating with our
                // service through an IDL interface, so get a client-side
                // representation of that from the raw service object.
                mService = new Messenger(service);

                // We want to monitor the service for as long as we are
                // connected to it.
                try {
                    Message msg = Message.obtain(null,
                            PredictionService.MSG_REGISTER_CLIENT);
                    msg.replyTo = mMessenger;
                    mService.send(msg);

                } catch (RemoteException e) {
                    // In this case the service has crashed before we could even
                    // do anything with it; we can count on soon being
                    // disconnected (and then reconnected if it can be restarted)
                    // so there is no need to do anything here.
                }
                unigrams = new UnigramDataSource(getBaseContext());
                mCorrection = new Correction(unigrams);
            }

            public void onServiceDisconnected(ComponentName className) {
                // This is called when the connection with the service has been
                // unexpectedly disconnected -- that is, its process crashed.
                mService = null;

                // As part of the sample, tell the user what happened.
                /*Toast.makeText(SoftKeyboard.this, "Remote service disconnected",
                        Toast.LENGTH_SHORT).show();*/
            }
        };
        //bind prediction Service
        doBindService();
        // preferenceChangeListener setup - detect preference changes requiring reinitializiation
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        prefs.registerOnSharedPreferenceChangeListener(this);
        mIsCorrectionOn = prefs.getBoolean("correction_on_off", true);
    }

    /**
     * This is the point where you can do all of your UI initialization.  It
     * is called after creation and any configuration change.
     */
    @Override public void onInitializeInterface() {
        Log.d("KB", "onInitializeInterface called!");
        if (mQwertyKeyboard != null) {
            // Configuration changes can happen after the keyboard gets recreated,
            // so we need to be able to re-build the keyboards if the available
            // space has changed.
            int displayWidth = getMaxWidth();
            if (displayWidth == mLastDisplayWidth) return;
            mLastDisplayWidth = displayWidth;
        }
        try {
            mQwertyKeyboard = new LatinKeyboard(this, R.xml.scanning);
            mSymbolsKeyboard = new LatinKeyboard(this, R.xml.symbols_scan);
            mSymbolsShiftedKeyboard = new LatinKeyboard(this, R.xml.symbols_shift);
            mScanningKeyboard = new LatinKeyboard(this, R.xml.scanning);
            mSymbolsScanKeyboard = new LatinKeyboard(this, R.xml.symbols_scan);
        } catch (XmlPullParserException e) {
            e.printStackTrace();
            //todo handle this better
            Log.e("KB", "Error initializing keyboard!");
        }
    }
    
    /**
     * Called by the framework when your view for creating input needs to
     * be generated.  This will be called the first time your input method
     * is displayed, and every time it needs to be re-created such as due to
     * a configuration change.
     */
    @Override public View onCreateInputView() {
        Log.d("KB", "onCreateInputView called!");
        mInputView = (LatinKeyboardView) getLayoutInflater().inflate(
                R.layout.input, null);
        mInputView.setOnKeyboardActionListener(this);
        mInputView.setPreferenceValue();
        if (mode != mInputView.MODE_NORMAL)
            mCurKeyboard = mScanningKeyboard;
        else
            mCurKeyboard = mQwertyKeyboard;
        setLatinKeyboard(mCurKeyboard);
        mInputView.initializeScanner();
        mInputView.toggleHandler();
        return mInputView;
    }

    @Override
    public void onUpdateExtractingVisibility(EditorInfo ei) {
        //ei.imeOptions |= EditorInfo.IME_FLAG_NO_EXTRACT_UI;
        //setExtractViewShown(false);
        super.onUpdateExtractingVisibility(ei);
    }

    @Override
    public boolean onEvaluateFullscreenMode() {
        return false;
    }

    private void setLatinKeyboard(LatinKeyboard nextKeyboard) {
        final boolean shouldSupportLanguageSwitchKey = true ;
        // mInputMethodManager.shouldOfferSwitchingToNextInputMethod(getToken());
        nextKeyboard.setLanguageSwitchKeyVisibility(shouldSupportLanguageSwitchKey);
        mInputView.setKeyboard(nextKeyboard);
        if (mCorrection != null)
            mCorrection.setLetters(nextKeyboard.getLetters());
    }

    /**
     * Called by the framework when your view for showing candidates needs to
     * be generated, like {@link #onCreateInputView}.
     */
    @Override public View onCreateCandidatesView() {
        mCandidateView = new CandidateView(this);
        mCandidateView.setService(this);
        CandidateScanner scanner = new CandidateScanner(mCandidateView, prefs);
        mCandidateView.setCandidateScanner(scanner);
        mInputView.setCandidateView(mCandidateView);
        return mCandidateView;
    }

    @Override
    public void onDestroy(){
        prefs.unregisterOnSharedPreferenceChangeListener(this);
        setCandidatesViewShown(false);
        if (mConnection != null && mIsBound)
            doUnbindService();
        unigrams.close();
        super.onDestroy();
    }

    /**
     * This is the main point where we do our initialization of the input method
     * to begin operating on an application.  At this point we have been
     * bound to the client, and are now receiving all of the detailed information
     * about the target of our edits.
     */
    @Override public void onStartInput(EditorInfo attribute, boolean restarting) {
        super.onStartInput(attribute, restarting);
        
        // Reset our state.  We want to do this even if restarting, because
        // the underlying state of the text editor could have changed in any way.
        mComposing.reset();
        updateCandidates();
        
        if (!restarting) {
            // Clear shift states.
            mMetaState = 0;
        }
        
        mPredictionOn = false;
        mCompletionOn = false;
        mCompletions = null;
        
        // We are now going to initialize our state based on the type of
        // text being edited.
        switch (attribute.inputType & InputType.TYPE_MASK_CLASS) {
            case InputType.TYPE_CLASS_NUMBER:
            case InputType.TYPE_CLASS_DATETIME:
                // Numbers and dates default to the symbols keyboard, with
                // no extra features.
                mCurKeyboard = mSymbolsKeyboard;
                break;
                
            case InputType.TYPE_CLASS_PHONE:
                // Phones will also default to the symbols keyboard, though
                // often you will want to have a dedicated phone keyboard.
                mCurKeyboard = mSymbolsKeyboard;
                break;
                
            case InputType.TYPE_CLASS_TEXT:
                // This is general text editing.  We will default to the
                // normal alphabetic keyboard, and assume that we should
                // be doing predictive text (showing candidates as the
                // user types).
                mCurKeyboard = mQwertyKeyboard;
                mPredictionOn = true;
                
                // We now look for a few special variations of text that will
                // modify our behavior.
                int variation = attribute.inputType & InputType.TYPE_MASK_VARIATION;
                if (variation == InputType.TYPE_TEXT_VARIATION_PASSWORD ||
                        variation == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD) {
                    // Do not display predictions / what the user is typing
                    // when they are entering a password.
                    mPredictionOn = false;
                }
                
                if (variation == InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
                        || variation == InputType.TYPE_TEXT_VARIATION_URI
                        || variation == InputType.TYPE_TEXT_VARIATION_FILTER) {
                    // Our predictions are not useful for e-mail addresses
                    // or URIs.
                    mPredictionOn = false;
                }
                
                if ((attribute.inputType & InputType.TYPE_TEXT_FLAG_AUTO_COMPLETE) != 0) {
                    // If this is an auto-complete text view, then our predictions
                    // will not be shown and instead we will allow the editor
                    // to supply their own.  We only show the editor's
                    // candidates when in fullscreen mode, otherwise relying
                    // own it displaying its own UI.
                    mPredictionOn = false;
                    mCompletionOn = isFullscreenMode();
                }
                
                // We also want to look at the current state of the editor
                // to decide whether our alphabetic keyboard should start out
                // shifted.
                updateShiftKeyState(attribute);
                break;
                
            default:
                // For all unknown input types, default to the alphabetic
                // keyboard with no special features.
                mCurKeyboard = mQwertyKeyboard;
                updateShiftKeyState(attribute);
        }
        
        // Update the label on the enter key, depending on what the application
        // says it will do.
        mCurKeyboard.setImeOptions(getResources(), attribute.imeOptions);
    }

    @Override 
    public void onFinishInputView(boolean finishingInput){
        super.onFinishInputView(true);
        mCandidateView.setVisibility(View.INVISIBLE);
        if (mInputView != null) {
            mInputView.closing();
        }
    }

    /**
     * This is called when the user is done editing a field.  We can use
     * this to reset our state.
     */
    @Override public void onFinishInput() {
        super.onFinishInput();
        // Clear current composing text and candidates.
        mComposing.reset();
        updateCandidates();
        
        // We only hide the candidates window when finishing input on
        // a particular editor, to avoid popping the underlying application
        // up and down if the user is entering text into the bottom of
        // its window.
        setCandidatesViewShown(false);
        //todo unbind PredictionService?
        //doUnbindService();
    }
    
    @Override public void onStartInputView(EditorInfo attribute, boolean restarting) {
        super.onStartInputView(attribute, restarting);

        final InputMethodSubtype subtype = mInputMethodManager.getCurrentInputMethodSubtype();
        mInputView.setSubtypeOnSpaceKey(subtype);
        mCandidateView.setVisibility(View.VISIBLE);
        // Apply the selected keyboard to the input view.
        mInputView.setPreferenceValue();
        if (mode != LatinKeyboardView.MODE_NORMAL) {
            mChangeDirTE = false;
            mSelectTE = false;
            mBackspaceTE = false;
            mEnterTE = false;
            setLatinKeyboard(mScanningKeyboard);
            mInputView.toggleHandler();
            if (mode == LatinKeyboardView.MODE_BISECT_ROWS || mode == LatinKeyboardView.MODE_BISECT_COLUMNS) {
                if (mInputView.mScanner != null) mInputView.mScanner.resetCounters();
            }
        } else {
            setLatinKeyboard(mCurKeyboard);
        }
    }

    @Override
    public void onCurrentInputMethodSubtypeChanged(InputMethodSubtype subtype) {
        mInputView.setSubtypeOnSpaceKey(subtype);
    }

    /**
     * Deal with the editor reporting movement of its cursor.
     */
    @Override public void onUpdateSelection(int oldSelStart, int oldSelEnd,
            int newSelStart, int newSelEnd,
            int candidatesStart, int candidatesEnd) {
        super.onUpdateSelection(oldSelStart, oldSelEnd, newSelStart, newSelEnd,
                candidatesStart, candidatesEnd);
        
        // If the current selection in the text view changes, we should
        // clear whatever candidate text we have.
        if (mComposing.size() > 0 && (newSelStart != candidatesEnd
                || newSelEnd != candidatesEnd)) {
            mComposing.reset();
            updateCandidates();
            InputConnection ic = getCurrentInputConnection();
            if (ic != null) {
                ic.finishComposingText();
            }
        }
    }

    /**
     * This tells us about completions that the editor has determined based
     * on the current text in it.  We want to use this in fullscreen mode
     * to show the completions ourself, since the editor can not be seen
     * in that situation.
     */
    @Override public void  onDisplayCompletions(CompletionInfo[] completions) {
        if (mCompletionOn) {
            mCompletions = completions;
            if (completions == null) {
                setSuggestions(null, false, false);
                return;
            }
            
            List<String> stringList = new ArrayList<String>();
            for (int i = 0; i < completions.length; i++) {
                CompletionInfo ci = completions[i];
                if (ci != null) stringList.add(ci.getText().toString());
            }
            setSuggestions(stringList, true, true);
        }
    }
    
    /**
     * This translates incoming hard key events in to edit operations on an
     * InputConnection.  It is only needed when using the
     * PROCESS_HARD_KEYS option.
     */
    private boolean translateKeyDown(int keyCode, KeyEvent event) {
        mMetaState = MetaKeyKeyListener.handleKeyDown(mMetaState,
                keyCode, event);
        int c = event.getUnicodeChar(MetaKeyKeyListener.getMetaState(mMetaState));
        mMetaState = MetaKeyKeyListener.adjustMetaAfterKeypress(mMetaState);
        InputConnection ic = getCurrentInputConnection();
        if (c == 0 || ic == null) {
            return false;
        }
        
        boolean dead = false;

        if ((c & KeyCharacterMap.COMBINING_ACCENT) != 0) {
            dead = true;
            c = c & KeyCharacterMap.COMBINING_ACCENT_MASK;
        }

        //todo: figure out what this method does translateKeyDown
        /*if (mComposing.size() > 0) {
            char accent = (char) mComposing.getCodesAt(mComposing.size() -1 )[0];
            int composed = KeyEvent.getDeadChar(accent, c);

            if (composed != 0) {
                c = composed;
                mComposing.deleteLast();
            }
        }*/
        
        onKey(c, null);
        
        return true;
    }
    
    /**
     * Use this to monitor key events being delivered to the application.
     * We get first crack at them, and can either resume them or let them
     * continue to the app.
     */
    @Override public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_HOME:
                setCandidatesViewShown(false);
                break;
            case KeyEvent.KEYCODE_BACK:
                // The InputMethodService already takes care of the back
                // key for us, to dismiss the input method if it is shown.
                // However, our keyboard could be showing a pop-up window
                // that back should dismiss, so we first allow it to do that.
                setCandidatesViewShown(false);
                if (event.getRepeatCount() == 0 && mInputView != null) {
                    if (mInputView.handleBack()) {
                        return true;
                    }
                }
                break;
                
            case KeyEvent.KEYCODE_DEL:
                // Special handling of the delete key: if we currently are
                // composing text for the user, we want to modify that instead
                // of let the application to the delete itself.
                if (mComposing.size() > 0) {
                    onKey(Keyboard.KEYCODE_DELETE, null);
                    return true;
                }
                break;
                
            case KeyEvent.KEYCODE_ENTER:
                //Let the underlying text editor use enter
                return false;
                
            default:
        }
        
        return super.onKeyDown(keyCode, event);
    }

    /**
     * Use this to monitor key events being delivered to the application.
     * We get first crack at them, and can either resume them or let them
     * continue to the app.
     */
    @Override public boolean onKeyUp(int keyCode, KeyEvent event) {
        // If we want to do transformations on text being entered with a hard
        // keyboard, we need to process the up events to update the meta key
        // state we are tracking.
        if (PROCESS_HARD_KEYS) {
            if (mPredictionOn) {
                mMetaState = MetaKeyKeyListener.handleKeyUp(mMetaState,
                        keyCode, event);
            }
        }

        switch(keyCode){
            case KeyEvent.KEYCODE_SPACE: //srednji TE
                mInputView.middleFunction();
                break;
            case KeyEvent.KEYCODE_B: //gore desni TE
                mBackspaceTE = true;
                break;
            case KeyEvent.KEYCODE_V: //dolje desni TE
                mEnterTE = true;
                break;
            case KeyEvent.KEYCODE_C: //dolje lijevi TE
                mInputView.lowerLeftFunction();
                break;
            case KeyEvent.KEYCODE_X: //gore lijevi TE
                mInputView.upperLeftFunction();
                break;
            case KeyEvent.KEYCODE_R:
                mInputView.resetScan();
        }
        if (LatinKeyboardView.mCurrentMode != LatinKeyboardView.MODE_NORMAL) {
            onKey(LatinKeyboardView.currentKey, null);
        }
        
        return super.onKeyUp(keyCode, event);
    }

    /**
     * Helper function to commit any text being composed in to the editor.
     */
    private void commitTyped(InputConnection inputConnection) {
        if (mComposing.size() > 0) {
            inputConnection.commitText(mComposing.getPreferredWord(), mComposing.size());
            mComposing.reset();
            updateCandidates();
        }
    }

    /**
     * Helper to update the shift state of our keyboard based on the initial
     * editor state.
     */
    private void updateShiftKeyState(EditorInfo attr) {
        if (attr != null 
                && mInputView != null && mQwertyKeyboard == mInputView.getKeyboard()) {
            int caps = 0;
            EditorInfo ei = getCurrentInputEditorInfo();
            if (ei != null && ei.inputType != InputType.TYPE_NULL) {
                caps = getCurrentInputConnection().getCursorCapsMode(attr.inputType);
            }
            mInputView.setShifted(mCapsLock || caps != 0);
        }
    }
    
    /**
     * Helper to determine if a given character code is alphabetic or a digit
     */
    private boolean isAlphanum(int code) {
        if (Character.isLetter(code) ||Character.isDigit(code)) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Helper to send a key down / key up pair to the current editor.
     */
    private void keyDownUp(int keyEventCode) {
        getCurrentInputConnection().sendKeyEvent(
                new KeyEvent(KeyEvent.ACTION_DOWN, keyEventCode));
        getCurrentInputConnection().sendKeyEvent(
                new KeyEvent(KeyEvent.ACTION_UP, keyEventCode));
    }
    
    /**
     * Helper to send a character to the editor as raw key events.
     */
    private void sendKey(int keyCode) {
        switch (keyCode) {
            case '\n':
                keyDownUp(KeyEvent.KEYCODE_ENTER);
                break;
            default:
                if (keyCode >= '0' && keyCode <= '9') {
                    keyDownUp(keyCode - '0' + KeyEvent.KEYCODE_0);
                } else {
                    getCurrentInputConnection().commitText(String.valueOf((char) keyCode), 1);
                }
                break;
        }
    }

    // Implementation of KeyboardViewListener
    public void onKey(int primaryCode, int[] keyCodes) {
        //mInputView.setPreferenceValue();
        if (isWordSeparator(primaryCode) && !mInputView.isScanningMode()) {
            // Handle separator
            if (mComposing.size() > 0) {
                //todo testing, remove later
                if (!isBeingUsedByKomunikator())
                    addSuggestionToService(mComposing.getTypedWord(), PredictionService.PREDICTION_TYPE_SESSION_BIGRAM);
                mPreviousWord = mComposing.getTypedWord();
                commitTyped(getCurrentInputConnection());
            }
            sendKey(primaryCode);
            updateShiftKeyState(getCurrentInputEditorInfo());
        } else if (primaryCode == Keyboard.KEYCODE_DELETE) {
            if (!mInputView.isScanningMode())
                handleBackspace();
        } else if (primaryCode == Keyboard.KEYCODE_SHIFT) {
            if (!mInputView.isScanningMode())
                handleShift();
        } else if (primaryCode == Keyboard.KEYCODE_CANCEL) {
            if (!mInputView.isScanningMode()) {
                handleClose();
                return;
            }
        } else if (primaryCode == LatinKeyboardView.KEYCODE_SETTINGS && !mInputView.isScanningMode()) {
            Intent myIntent = new Intent(this, ImePreferences.class);
            myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(myIntent);
        } else if (primaryCode == Keyboard.KEYCODE_MODE_CHANGE && mInputView != null && !mInputView.isScanningMode()) {
            Keyboard current = mInputView.getKeyboard();
            if (current == mSymbolsKeyboard || current == mSymbolsShiftedKeyboard || current == mScanningKeyboard) {
                setLatinKeyboard(mQwertyKeyboard);
                mInputView.toggleHandler();
            } else {
                setLatinKeyboard(mSymbolsKeyboard);
                mSymbolsKeyboard.setShifted(false);
            }
        } else {
            if (mInputView.isScanningMode()) {
                if (mSelectTE){
                    mSelectTE = false;
                    if (isWordSeparator(LatinKeyboardView.currentKey)) {
                        // Handle separator
                        if (mComposing.size() > 0) {
                            if (!isBeingUsedByKomunikator())
                                addSuggestionToService(mComposing.getTypedWord(), PredictionService.PREDICTION_TYPE_SESSION_BIGRAM);
                            mPreviousWord = mComposing.getTypedWord();
                            commitTyped(getCurrentInputConnection());
                        }
                        sendKey(LatinKeyboardView.currentKey);
                        updateShiftKeyState(getCurrentInputEditorInfo());
                        //resetSelection();
                        mChangeDirTE = false;
                    } else if (LatinKeyboardView.currentKey == Keyboard.KEYCODE_CANCEL) {
                        handleClose();
                        return;
                    } else if (LatinKeyboardView.currentKey == Keyboard.KEYCODE_MODE_CHANGE){
                        Keyboard current = mInputView.getKeyboard();
                        if (current == mSymbolsScanKeyboard) {
                            mInputView.toggleHandler();
                            mChangeDirTE = false;
                            if (mInputView.mScanner != null){
                                mInputView.mScanner.resetCounters();
                            }
                            setLatinKeyboard(mScanningKeyboard);
                        } else {
                            mInputView.toggleHandler();
                            if (mInputView.mScanner != null) mInputView.mScanner.resetCounters();
                            mChangeDirTE = false;
                            setLatinKeyboard(mSymbolsScanKeyboard);
                        }
                    } else if (LatinKeyboardView.currentKey == LatinKeyboardView.KEYCODE_SETTINGS) {
                        Intent myIntent = new Intent(this, ImePreferences.class);
                        myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(myIntent);
                    } else if (LatinKeyboardView.currentKey == Keyboard.KEYCODE_SHIFT) {
                        handleShift();
                        mChangeDirTE = false;
                    } else if (LatinKeyboardView.currentKey == LatinKeyboardView.KEYCODE_TOGGLE_PREDICTION) {
                        mInputView.upperLeftFunction();
                    } else if (LatinKeyboardView.currentKey == LatinKeyboardView.KEYCODE_CHANGE_DIRECTION){
                        mInputView.lowerLeftFunction();
                    } else if (LatinKeyboardView.currentKey == LatinKeyboardView.KEYCODE_RESET_SCAN) {
                        mInputView.resetScan();
                    } else {
                        handleCharacter(LatinKeyboardView.currentKey, null);
                    }
                } else if (mBackspaceTE) {
                    mBackspaceTE = false;
                    handleBackspace();
                } else if (mEnterTE) {
                    mEnterTE = false;
                    sendKey(ENTER_KEY);
                }

            } else
                handleCharacter(primaryCode, keyCodes);
        }
    }

    public void onText(CharSequence text) {
        InputConnection ic = getCurrentInputConnection();
        if (ic == null) return;
        ic.beginBatchEdit();
        if (mComposing.size() > 0) {
            commitTyped(ic);
        }
        ic.commitText(text, 0);
        ic.endBatchEdit();
        updateShiftKeyState(getCurrentInputEditorInfo());
    }

    /**
     * Update the mSuggestions of available candidates from the current composing
     * text.  This will need to be filled in by however you are determining
     * candidates.
     */
    private void updateCandidates() {
        if (!mCompletionOn) {
            if (mComposing.size() >= 2) {
                mSuggestions.clear();
                askSuggestionsFromService(mComposing.getTypedWord(), PredictionService.PREDICTION_TYPE_ALL);
            }
            else if (mComposing.size() == 1){
                mSuggestions.clear();
                askSuggestionsFromService(mComposing.getTypedWord(), PredictionService.PREDICTION_TYPE_ALL_BIGRAM);
            }
            else {
                setSuggestions(null, false, false);
            }
        }
    }
    
    public void setSuggestions(List<String> suggestions, boolean completions,
            boolean typedWordValid) {
        if (suggestions != null && suggestions.size() > 0) {
            setCandidatesViewShown(true);
        } else if (isExtractViewShown()) {
            setCandidatesViewShown(true);
        }
        if (mCandidateView != null) {
            mCandidateView.setSuggestions(suggestions, completions, typedWordValid);
        }
    }
    
    public void handleBackspace() {
        final int length = mComposing.size();
        if (length > 1) {
            mComposing.deleteLast();
            getCurrentInputConnection().setComposingText(mComposing.getPreferredWord(), 1);
            updateCandidates();
        } else if (length > 0) {
            mComposing.reset();
            getCurrentInputConnection().commitText("", 0);
            if (getCurrentInputConnection().getTextBeforeCursor(1,0) == null){
                mPreviousWord = null;
            }
            updateCandidates();

        } else {
            //CharSequence char_text = getCurrentInputConnection().getTextBeforeCursor(50, 0);
            keyDownUp(KeyEvent.KEYCODE_DEL);
            /*if (char_text != null && char_text.length() > 0) {
                String text = char_text.toString();
                if (text.length() > 0 && text.charAt(text.length() - 1) == ' ') {
                    text = text.substring(0, text.length() - 1);
                    int start = text.lastIndexOf(' ');
                    start = start > 0 ? start + 1 : 0;
                    String searchToken = text.substring(start, text.length());
                    mComposing.setCurrentWord(searchToken);
                    getCurrentInputConnection().deleteSurroundingText(text.length() - start, 0);
                    getCurrentInputConnection().setComposingText(mComposing.getPreferredWord(), 1);
                    updateCandidates();
                }
            }*/
        }
        updateShiftKeyState(getCurrentInputEditorInfo());
        //resetSelection();
    }

    private void handleShift() {
        if (mInputView == null) {
            return;
        }
        
        Keyboard currentKeyboard = mInputView.getKeyboard();
        if (mQwertyKeyboard == currentKeyboard || mScanningKeyboard == currentKeyboard) {
            // Alphabet keyboard
            checkToggleCapsLock();
            mInputView.setShifted(mCapsLock || !mInputView.isShifted());
        } else if (currentKeyboard == mSymbolsKeyboard) {
            mSymbolsKeyboard.setShifted(true);
            setLatinKeyboard(mSymbolsShiftedKeyboard);
            mSymbolsShiftedKeyboard.setShifted(true);
        } else if (currentKeyboard == mSymbolsShiftedKeyboard) {
            mSymbolsShiftedKeyboard.setShifted(false);
            setLatinKeyboard(mSymbolsKeyboard);
            mSymbolsKeyboard.setShifted(false);
        }
    }

    public void handleCharacter(int primaryCode, int[] keyCodes) {
        if (isInputViewShown()) {
            if (mInputView.isShifted()) {
                primaryCode = Character.toUpperCase(primaryCode);
            }
        }
        if (isAlphanum(primaryCode) && mPredictionOn) {
            mComposing.add(primaryCode, keyCodes);
            getCurrentInputConnection().setComposingText(mComposing.getPreferredWord(), 1);
            updateShiftKeyState(getCurrentInputEditorInfo());
            updateCandidates();
        } else {
            getCurrentInputConnection().commitText(
                    String.valueOf((char) primaryCode), 1);
        }
        mChangeDirTE = false;
    }

    public void resetSelection(){ //reset selection of key for scanning mode
        mInputView.resetSelection();
        mChangeDirTE = false;
    }

    private void handleClose() {
        commitTyped(getCurrentInputConnection());
        requestHideSelf(0);
        resetSelection();
        mInputView.viewHandler.removeCallbacks(mInputView.updateView);
        mInputView.closing();
        prefs.unregisterOnSharedPreferenceChangeListener(this);

    }

    private IBinder getToken() {
        final Dialog dialog = getWindow();
        if (dialog == null) {
            return null;
        }
        final Window window = dialog.getWindow();
        if (window == null) {
            return null;
        }
        return window.getAttributes().token;
    }

    private void handleLanguageSwitch() {
        //mInputMethodManager.switchToNextInputMethod(getToken(), false /* onlyCurrentIme */);
    }

    private void checkToggleCapsLock() {
        long now = System.currentTimeMillis();
        if (mLastShiftTime + 800 > now) {
            mCapsLock = !mCapsLock;
            mLastShiftTime = 0;
        } else {
            mLastShiftTime = now;
        }
    }
    
    private String getWordSeparators() {
        return mWordSeparators;
    }
    
    public boolean isWordSeparator(int code) {
        String separators = getWordSeparators();
        return separators.contains(String.valueOf((char)code));
    }

    public void pickDefaultCandidate() {
        pickSuggestionManually(0);
    }
    
    public void pickSuggestionManually(int index) {
        if (mCompletionOn && mCompletions != null && index >= 0
                && index < mCompletions.length) {
            CompletionInfo ci = mCompletions[index];
            getCurrentInputConnection().commitCompletion(ci);
            if (mCandidateView != null) {
                mCandidateView.clear();
            }
            updateShiftKeyState(getCurrentInputEditorInfo());
        } else if (mComposing.size() > 0) {
            // Commit candidate suggestion
            mComposing.setPreferredWord(mSuggestions.getSorted().get(index));
            //todo refactor later?
            mPreviousWord = mComposing.getPreferredWord();
            //addSuggestionToService(mSuggestions.get(index), PredictionService.PREDICTION_TYPE_SESSION_UNIGRAM);
            commitTyped(getCurrentInputConnection());
            if (mCandidateView != null) {
                mCandidateView.clear();
            }
        }
    }
    
    public void swipeRight() {
        if (mCompletionOn) {
            pickDefaultCandidate();
        }
    }
    
    public void swipeLeft() {
        if(!mInputView.isScanningMode())
            handleBackspace();
    }

    public void swipeDown() {
        if(!mInputView.isScanningMode())
            handleClose();
    }

    public void swipeUp() {
    }
    
    public void onPress(int primaryCode) {
    }
    
    public void onRelease(int primaryCode) {
    }

    public WordComposer getCurrentWord(){
        return mComposing;
    }

    public boolean isScanningKeyboard(){
        return mInputView.isScanningMode();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        Log.d("KB", "preference changed!");
        mInputView.setPreferenceValue();
        //todo make "mode" a constant in preferences?
        if (s.equals("mode")) {
            Log.d("KB", "mode changed!");
            Log.d("KB", "current mode" + prefs.getString("mode", Integer.toString(0)));
            mInputView.initializeScanner();
        }
        mIsCorrectionOn = prefs.getBoolean("correction_on_off", true);

    }

    /// SERVICE HANDLING ///

    /**
     * Handler of incoming messages from service.
     */
    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case PredictionService.MSG_SET_SUGGESTIONS:
                    mSuggestions.clear();
                    Bundle data = msg.getData();
                    data.setClassLoader(SuggestedWords.class.getClassLoader());
                    mSuggestions = data.getParcelable("suggestions");
                    if (mIsCorrectionOn)
                        setSuggestions(mCorrection.correct(mComposing.getTypedWord(), mSuggestions), false, true);
                    else
                        setSuggestions(mSuggestions.getSorted(), false, true);
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }

    void doBindService() {
        // Establish a connection with the service.  We use an explicit
        // class name because there is no reason to be able to let other
        // applications replace our component.
        bindService(new Intent(this,
                PredictionService.class), mConnection, Context.BIND_AUTO_CREATE);
        mIsBound = true;
    }

    void doUnbindService() {
        if (mIsBound) {
            // If we have received the service, and hence registered with
            // it, then now is the time to unregister.
            if (mService != null) {
                try {
                    Message msg = Message.obtain(null,
                            PredictionService.MSG_UNREGISTER_CLIENT);
                    msg.replyTo = mMessenger;
                    mService.send(msg);
                } catch (RemoteException e) {
                    // There is nothing special we need to do if the service
                    // has crashed.
                }
            }

            // Detach our existing connection.
            unbindService(mConnection);
            mIsBound = false;
        }
    }

    private void askSuggestionsFromService(String word, int type){

        Message msg = Message.obtain(null,
                PredictionService.MSG_GET_VALUE, type, 0);
        Bundle data = new Bundle();
        data.putString("word", word);
        data.putString("previous", mPreviousWord);
        msg.setData(data);
        msg.replyTo = mMessenger;
        try {
            if (mService != null)
                mService.send(msg);
        } catch (RemoteException e) {
            Log.e("SOFT_KEYBOARD", "Remote exception" + e.getMessage());
            e.printStackTrace();
        }
    }

    private void addSuggestionToService(String word, int type){
        Message msg = Message.obtain(null,
                PredictionService.MSG_SET_VALUE, type, 0);
        Bundle data = new Bundle();
        data.putString("word", word);
        data.putString("previous", mPreviousWord);
        msg.setData(data);
        try {
            if (mService != null)
                mService.send(msg);
            Log.d("SOFT_KEYBOARD", "Word sent" + word);
            Log.d("SOFT_KEYBOARD", "Previous word sent" + word);
        } catch (RemoteException e) {
            Log.e("SOFT_KEYBOARD", "Remote exception" + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Provjera ako Komunikator aplikacija koristi tipkovnicu.
     * Ukoliko je to slučaj podaci iz tipkovnice se neće pohranjivati (da se izbjegne duplikacija
     * frekvencija)
     *
     * @return
     */
    private boolean isBeingUsedByKomunikator(){
        try {
            String process = getProcess();
            if (KOMUNIKATOR_PACKAGE_NAME == process) {
                return true;
            }
        }
        catch ( Exception e){
            Log.e("AKEYBOARD", "Exception in finding top PACKAGE NAME!");
        }
        return false;
    }


    private String getProcess() throws Exception {
        Log.d("SOFT_KEY",String.valueOf(Build.VERSION.SDK_INT) );
        if (Build.VERSION.SDK_INT >= 21) {
            return getProcessNew();
        } else {
            return getProcessOld();
        }
    }

    //API 21 and above
    @SuppressLint("NewApi")
    private String getProcessNew() throws Exception {
        String topPackageName = null;
        //noinspection ResourceType
        UsageStatsManager usage = (UsageStatsManager) this.getSystemService("usagestats");
        long time = System.currentTimeMillis();
        List<UsageStats> stats = usage.queryUsageStats(UsageStatsManager.INTERVAL_BEST, time - 10000, time);
        if (stats != null && stats.size() != 0) {
            SortedMap<Long, UsageStats> runningTask = new TreeMap<Long,UsageStats>();
            for (UsageStats usageStats : stats) {
                runningTask.put(usageStats.getLastTimeUsed(), usageStats);
            }
            if (runningTask.isEmpty()) {
                return null;
            }
            topPackageName =  runningTask.get(runningTask.lastKey()).getPackageName();
        }
        //Log.d("SOFT_KEY", "Found top package" + topPackageName);
        return topPackageName;
    }

    //API below 21
    @SuppressWarnings("deprecation")
    private String getProcessOld() throws Exception {
        String topPackageName = null;
        ActivityManager activity = (ActivityManager) this.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> runningTask = activity.getRunningTasks(1);
        if (runningTask != null) {
            ActivityManager.RunningTaskInfo taskTop = runningTask.get(0);
            ComponentName componentTop = taskTop.topActivity;
            topPackageName = componentTop.getPackageName();
        }
        return topPackageName;
    }
}