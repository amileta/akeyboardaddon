package com.example.android.softkeyboard;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.inputmethodservice.Keyboard;
import android.media.AudioManager;


/**
 * Created by Antonio on 6.12.2015..
 */
public class KeyboardScanner {

    public static boolean isSoundOn;

    protected Context mContext;

    protected LatinKeyboard mKeyboard;

    protected Keyboard.Key mCurrentKey;

    protected boolean mResetAfterSelect = true;

    protected Paint mBorderZonePaint;


    public void setResetAfterSelect(boolean setting){
        this.mResetAfterSelect = setting;
    }

    public void scan(){
        /**
         * Override with custom implementation of scanning (do counting etc.)
         */
        if (isSoundOn){
            AudioManager am = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
            am.playSoundEffect(AudioManager.FX_FOCUS_NAVIGATION_DOWN, 1);
        }
    }

    public void draw(Canvas canvas){
        /**
         * Override with custom implementation of drawing by taking into account current state
         */
    }

    public boolean nextLevel(){
        /**
         * Override with custom implementation for recursing into the next level
         */
        return false;
    }

    public void resetCounters() {
        /**
         * Override with custom implementation for resetting the counters
         */
    }

    public void toggleDirection(){
        /**
         * Override with custom implementation for changing scanning direction
         */
    }

    public void setKeyboard(LatinKeyboard keyboard){
        mKeyboard = keyboard;
    }

    public KeyboardScanner(LatinKeyboard keyboard, Context context) {
        mContext = context;
        mKeyboard = keyboard;
        mBorderZonePaint = new Paint();
        mBorderZonePaint.setStyle(Paint.Style.STROKE);
        mBorderZonePaint.setStrokeWidth(1);
        mBorderZonePaint.setColor(Color.WHITE);
    }


    public int getCurrentKey(){
        if (mCurrentKey != null)
            return mCurrentKey.codes[0];
        else
            return LatinKeyboard.NO_KEY;
    }

    protected void drawBorderHighlight(Canvas canvas){

    }

    protected void drawBorderHighlightInner(Canvas canvas){

    }

}
