package com.example.android.softkeyboard;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.inputmethodservice.Keyboard;

import java.util.Collections;

/**
 * Created by Antonio on 25.12.2015..
 */
public class KeyboardLinearScanner extends KeyboardScanner {

    protected static int INDEX_START = -1;
    protected static int DIRECTION_RIGHT = 1;
    protected static int DIRECTION_LEFT = 2;

    protected int direction = DIRECTION_RIGHT;

    protected int mCurrentRow = 0;
    protected int mCurrentColumn = 0;

    protected Drawable mHighlight;

    protected Keyboard.Key left_top;
    protected Keyboard.Key right_bottom;

    public KeyboardLinearScanner(LatinKeyboard keyboard, Context context) {
        super(keyboard, context);
        mHighlight = context.getResources().getDrawable(R.drawable.dark_green_tint);
        if (direction == DIRECTION_LEFT){
            mCurrentColumn = Collections.max(mKeyboard.mColumnsNum);
            mCurrentRow = mKeyboard.mRowsNum;
        }
    }

    @Override
    public void setKeyboard(LatinKeyboard keyboard){
        super.setKeyboard(keyboard);
        left_top = mKeyboard.getKey(0,0);
        right_bottom = mKeyboard.getLastKey();
    }


    @Override
    public void resetCounters(){
        mCurrentRow = 0;
        mCurrentColumn = 0;
        direction = DIRECTION_RIGHT;
    }


    @Override
    public void toggleDirection(){
        if (direction == DIRECTION_RIGHT){
            direction = DIRECTION_LEFT;
        } else {
            direction = DIRECTION_RIGHT;
        }
    }

    protected void drawBorderHighlight(Canvas canvas){
        Rect rect = new Rect(
                left_top.x, left_top.y,
                right_bottom.x+right_bottom.width,
                right_bottom.y+right_bottom.height);
        //highlight keyboard bounds
        canvas.drawRect(rect, mBorderZonePaint);
    }

}
