package com.example.android.softkeyboard;

import com.prediction.SuggestedWords;
import com.prediction.DataSource;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

public class Correction {

    //default querty layout, used when no other more specific layout is available

    private char[][] DEFAULT_LAYOUT = {
            {'q','w','e','r','t','z','u','i','o','p'},
            {'a','s','d','f','g','h','j','k','l'},
            {'\0','y','x','c','v','b','n','m'},
            {'\0',',',' ',' ',' ',' ',' ','.'}
    };

    private TreeMap<Character, ArrayList<Character>> adj_list;
    private DataSource dictionary;

    Correction(DataSource dictionary) {
        this.dictionary = dictionary;
        this.adj_list = new TreeMap<>();
        setLetters(DEFAULT_LAYOUT);
    }

    public Correction(char[][] letters, DataSource dictionary) {
        this.dictionary = dictionary;
        this.adj_list = new TreeMap<>();
        setLetters(letters);
    }


    private boolean isValid(char c){
        if (c != '\0')
            return true;
        return false;
    }

    public void setLetters(char[][] letters){
        adj_list.clear();
        for (int i = 0; i < letters.length; i++){
            for (int j = 0; j < letters[i].length; j++){
                ArrayList<Character> list = new ArrayList<Character>();
                if (letters[i][j] != '\0'){
                    int next_row = i+1;
                    int prev_row = i-1;
                    int next_col = j+1;
                    int prev_col = j-1;
                    if (i > 0 && j < letters[prev_row].length && isValid(letters[prev_row][j])) {
                        list.add(letters[prev_row][j]);
                    }
                    if (j > 0 && isValid(letters[i][prev_col])) {
                        list.add(letters[i][prev_col]);
                    }
                    if (i > 0 && j > 0 && isValid(letters[prev_row][prev_col])){
                        list.add(letters[prev_row][prev_col]);
                    }
                    if (next_row < letters.length && j < letters[next_row].length && isValid(letters[next_row][j])){
                        list.add(letters[next_row][j]);
                    }
                    if (next_col < letters[i].length && isValid(letters[i][next_col])){
                        list.add(letters[i][next_col]);
                    }
                    if (next_row < letters.length && j+1 < letters[next_row].length && isValid(letters[next_row][next_col])){
                        list.add(letters[next_row][next_col]);
                    }
                    if (i > 0 && next_col < letters[prev_row].length && isValid(letters[prev_row][next_col])){
                        list.add(letters[prev_row][next_col]);
                    }
                    if (next_row < letters.length && j > 0 && j < letters[next_row].length && isValid(letters[next_row][prev_col])){
                        list.add(letters[next_row][prev_col]);
                    }

                    adj_list.put(letters[i][j], list);
                }
            }
        }
    }

	public List<String> correct(final String word, SuggestedWords suggestions){

        if (!dictionary.open()){
            return suggestions.getSorted(true);
        }

        if (adj_list.size() == 0 || word == null || word.length() <= 1){
            return suggestions.getSorted();
        }

        StringBuilder word_suggestion = new StringBuilder(word);
        String word_double_lett_correction = word;
        char letter;
        int k = 0;
        int freq;
        for (int j=0; j < word_suggestion.length(); j++){
            word_suggestion = new StringBuilder(word);
            letter = word.charAt(j);
            if (k+1 < word_double_lett_correction.length() && letter == word.charAt(j+1)){
                word_double_lett_correction = word_double_lett_correction.substring(0,k) + word_double_lett_correction.substring(k + 1);
                if ((freq = dictionary.getFrequency(word_double_lett_correction)) > 0){
                    suggestions.add(word_double_lett_correction, freq);
                }
            } else {
                k++;
            }
            ArrayList<Character> neighbours = adj_list.get(letter);
            if (neighbours != null) {
                for (Character neighbour : neighbours) {
                    word_suggestion.setCharAt(j, neighbour);
                    if ((freq = dictionary.getFrequency(word_suggestion.toString())) > 0)
                        suggestions.add(word_suggestion.toString(), freq);
                }
            }
        }
        return suggestions.getSorted();
    }

}
