package com.example.android.softkeyboard;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.inputmethodservice.Keyboard;
import android.util.Log;

import java.util.Collections;

/**
 * Created by Antonio on 6.12.2015..
 */
public class KeyboardColumnScanner extends KeyboardLinearScanner {

    public boolean isInColumn = false;

    //todo use an interface instead of LatinKeyboard class for greater future flexibility
    public KeyboardColumnScanner(LatinKeyboard keyboard, Context context) {
        super(keyboard, context);
    }

    @Override
    public void draw(Canvas canvas){
        drawByColumns(canvas);
    }

    @Override
    public void scan(){
        super.scan();
        if (isInColumn){
            if (mKeyboard.isValidKey(mCurrentRow, mCurrentColumn)){
                //increment row counter
                if (direction == DIRECTION_RIGHT) {
                    mCurrentRow = (mCurrentRow + 1) % mKeyboard.getRowsNum();
                } else {
                    mCurrentRow = (mCurrentRow - 1) % mKeyboard.getRowsNum();
                    if (mCurrentRow == INDEX_START)
                        mCurrentRow = mKeyboard.getRowsNum() - 1;
                }
            }
        } else {
            //increment column counter
            if (direction == DIRECTION_RIGHT) {
                mCurrentColumn = (mCurrentColumn + 1) % Collections.max(mKeyboard.getColumnsNum());
            } else {
                mCurrentColumn = (mCurrentColumn - 1) % Collections.max(mKeyboard.getColumnsNum());
                if (mCurrentColumn == INDEX_START)
                    mCurrentColumn = Collections.max(mKeyboard.getColumnsNum()) - 1;
            }
        }
    }
    

    @Override
    public boolean nextLevel(){
        //keypress happened
        if (isInColumn) {
            isInColumn = false;
            if (mResetAfterSelect) {
                mCurrentColumn = 0;
            }
            mCurrentRow = 0;
            direction = DIRECTION_RIGHT;
            return true;
        }
        //reset direction
        direction = DIRECTION_RIGHT;
        //enter column
        isInColumn = true;

        if (mResetAfterSelect) {
            mCurrentRow = 0;
        }
        return false;
    }

    @Override
    public void resetCounters(){
        super.resetCounters();
        isInColumn = false;
    }

    private void drawByColumns(Canvas canvas){

        if (isInColumn){
            if (mKeyboard.isValidKey(mCurrentRow,mCurrentColumn)){
                //get the current key
                Keyboard.Key key = mKeyboard.getKey(mCurrentRow,mCurrentColumn);
                mCurrentKey = key;
                //Log.d("KB", "Current key:" + LatinKeyboardView.currentKey);
                //set the mHighlight bounds
                mHighlight.setBounds(key.x, key.y, key.x + key.width, key.y + key.height);
                mHighlight.draw(canvas);
                drawBorderHighlightInner(canvas);
            }
        }
        else {
            //mHighlight all keys in rows from current column, ignore if row does not contain current
            //column
            for (int i=0; i < mKeyboard.getRowsNum(); i++){
                //get the current key
                if (mKeyboard.isValidKey(i,mCurrentColumn)) {
                    Keyboard.Key key = mKeyboard.getKey(i,mCurrentColumn);
                    //set the mHighlight bounds
                    mHighlight.setBounds(key.x, key.y, key.x + key.width, key.y + key.height);
                    mHighlight.draw(canvas);
                }
            }
            drawBorderHighlight(canvas);
        }

    }

    @Override
    protected void drawBorderHighlightInner(Canvas canvas){
        //get the first and last key in column
        Keyboard.Key first = mKeyboard.getKey(0,mCurrentColumn);
        Keyboard.Key last = mKeyboard.getKey(mKeyboard.getRowsNum()-1,mCurrentColumn);
        //highlight keyboard bounds
        Rect rect = new Rect(
                first.x, first.y,
                last.x+last.width,
                last.y+last.height);
        canvas.drawRect(rect, mBorderZonePaint);
    }

}
