package com.android.inputmethodcommon;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.softkeyboard.R;

import java.util.ArrayList;


/**
 * Created by Antonio on 26.12.2015..
 */
public class CustomListPreference extends ListPreference {

    private static int INVALID = -1;
    private static int CUSTOM_ROW_INDEX = 5;

    private CharSequence[] entries;
    private CharSequence[] entryValues;


    private Context mContext;

    private ArrayList<RadioButton> rButtonList;
    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;

    private CustomListPreferenceAdapter customListPreferenceAdapter;

    private String mTime = "1 s";

    public CustomListPreference(Context context, AttributeSet attrs){
        super(context, attrs);
        mContext = context;
        prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        editor = prefs.edit();
        rButtonList = new ArrayList<>();

    }

    @Override
    protected void onPrepareDialogBuilder(AlertDialog.Builder builder){
        entries = getEntries();
        entryValues = getEntryValues();

        if (entries == null ||entryValues == null || entries.length != entryValues.length ){
            throw new IllegalStateException(
                    "ListPreference requires an entries array and values array matched in length!"
            );
        }

        customListPreferenceAdapter = new CustomListPreferenceAdapter(mContext);
        for (int i = 0; i < entries.length; i++){
            customListPreferenceAdapter.addItem(entryValues[i].toString());
            customListPreferenceAdapter.addLabel(entries[i].toString());
        }
        //add custom value from prefs
        customListPreferenceAdapter.addCustomItem(prefs.getString("lettspeed", "1000"));
        builder.setAdapter(customListPreferenceAdapter, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.setPositiveButton("OK", null);


    }

    @Override
    protected void showDialog(Bundle state) {
        /**
         * Overriding this so we can alert user when validation fails
         **/
        super.showDialog(state);

        final AlertDialog dialog = (AlertDialog) getDialog();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (customListPreferenceAdapter.getCheckedRow() == CUSTOM_ROW_INDEX) {
                    Log.d("Editor", "validate " + customListPreferenceAdapter.mCustomEditText.getText());
                    if (!customListPreferenceAdapter.validateTime(customListPreferenceAdapter.mCustomEditText.getText())){
                        Toast.makeText(mContext, "Time must be between 0.3 and 5 seconds", Toast.LENGTH_SHORT).show();
                        return;
                    }
                } else {
                    // put into shared preferences
                    String value = customListPreferenceAdapter.getCheckedData();
                    editor.putString(mContext.getString(R.string.lettspeed), value);
                }
                editor.commit();
                setSummary(mTime);
                dialog.dismiss();
            }
        });
    }

    private class CustomListPreferenceAdapter extends BaseAdapter{

        private int checkedRow = INVALID;

        private static final int TYPE_ITEM = 0;
        private static final int TYPE_CUSTOM = 1;
        private static final int TYPE_MAX_COUNT = TYPE_CUSTOM + 1;

        //this is where we store the data currently in Adapter
        private ArrayList<String> mData = new ArrayList();
        private ArrayList<String> mLabels = new ArrayList();

        private LayoutInflater inflater;
        private SharedPreferences prefs;

        //this is where we store index of our custom row
        private int customRow;

        private EditText mCustomEditText;

        public CustomListPreferenceAdapter(Context context){
            this.prefs = PreferenceManager.getDefaultSharedPreferences(context);
            this.inflater = LayoutInflater.from(mContext);
        }

        public void addItem(final String item){
            mData.add(item);
            notifyDataSetChanged();
        }

        public void addLabel(final String item){
            mLabels.add(item);
            notifyDataSetChanged();
        }

        public void addCustomItem(final String customItem){
            mData.add(customItem);
            mLabels.add("" + Float.parseFloat(customItem)/1000);
            //save position
            customRow = (mData.size()-1);
            notifyDataSetChanged();
        }

        @Override
        public int getViewTypeCount() {
            return TYPE_MAX_COUNT;
        }

        @Override
        public int getItemViewType(int position) {
            return (position == customRow)? TYPE_CUSTOM : TYPE_ITEM;
        }

        @Override
        public int getCount() {
            return mData.size();
        }

        @Override
        public Object getItem(int i) {
            return mData.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {

            int type = getItemViewType(i);
            View row = view;

            switch (type) {
                case TYPE_ITEM:
                    Holder holder;
                    if (row == null) {
                        //inflate a normal holder item
                        holder = new Holder();
                        row = inflater.inflate(R.layout.scan_time_list_preference_normal, viewGroup, false);
                        holder.text = (TextView) row.findViewById(R.id.list_view_row_text_view);
                        holder.text.setText(mLabels.get(i));
                        holder.rButton = (RadioButton) row.findViewById(R.id.list_view_row_radio_button);
                        holder.rButton.setId(i);
                        holder.rButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                            @Override
                            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                if (b) {
                                    //reset all others
                                    for (RadioButton rb : rButtonList) {
                                        if (rb.getId() != compoundButton.getId())
                                            rb.setChecked(false);
                                        else {
                                            checkedRow = rb.getId();
                                        }
                                    }
                                    mTime = mLabels.get(checkedRow);
                                }
                            }
                        });
                        rButtonList.add(holder.rButton);
                        if (prefs.getString(mContext.getString(R.string.lettspeed), "1000").equals(mData.get(i)))
                            holder.rButton.setChecked(true);
                        else
                            holder.rButton.setChecked(false);
                        row.setTag(holder);
                    } else {
                        //set data
                        holder = (Holder) row.getTag();
                        holder.text.setText(mLabels.get(i));
                        holder.rButton.setId(i);
                    }
                    break;
                case TYPE_CUSTOM:
                    CustomHolder customHolder;
                    if (row == null) {
                        customHolder = new CustomHolder();
                        row = inflater.inflate(R.layout.scan_time_list_preference, viewGroup, false);
                        if (mCustomEditText == null) {
                            mCustomEditText = (EditText) row.findViewById(R.id.custom_list_view_row_edit_text);
                            customHolder.text = mCustomEditText;
                        } else {
                            customHolder.text = mCustomEditText;
                        }
                        customHolder.rButton = (RadioButton) row.findViewById(R.id.custom_list_view_row_radio_button);
                        customHolder.rButton.setId(i);
                        customHolder.rButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                            @Override
                            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                if (b) {
                                    //deselect the previously checked
                                    for (RadioButton rb : rButtonList) {
                                        if (compoundButton.getId() != rb.getId())
                                            rb.setChecked(false);
                                        else
                                            checkedRow = compoundButton.getId();
                                    }
                                    mTime = mLabels.get(checkedRow);
                                }
                            }
                        });
                        rButtonList.add(customHolder.rButton);
                        if (!mData.subList(0, mData.size() - 1).contains(mData.get(customRow))) {
                            customHolder.rButton.setChecked(true);
                            customHolder.text.setText(mLabels.get(customRow).toString());
                        }
                        row.setTag(customHolder);
                    } else {
                        customHolder = (CustomHolder) row.getTag();
                        mCustomEditText = customHolder.text;
                        customHolder.rButton.setId(i);
                    }
                    break;
            }
            row.setClickable(true);
            row.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    Holder holder  = (Holder) view.getTag();
                    RadioButton button = holder.rButton;
                    // uncheck other buttons
                    for(RadioButton rb : rButtonList){
                        if (button.getId() != rb.getId())
                            rb.setChecked(false);
                        else
                            checkedRow = button.getId();
                    }
                    holder.rButton.setChecked(true);
                }
            });
            return row;
        }

        public int getCheckedRow() {
            return checkedRow;
        }

        public String getCheckedData(){
            return mData.get(checkedRow);
        }

        class Holder {
            public TextView text = null;
            public RadioButton rButton = null;
        }

        class CustomHolder extends Holder {
            public EditText text;
            public CustomHolder() {
                getDialog().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
            }
        }

        private boolean validateTime(Editable time) {
            try {
                Integer int_time = (int) (Float.parseFloat(time.toString()) * 1000);
                if (int_time < 300 || int_time > 5000 ){
                    return false;
                }
                editor.putString(mContext.getString(R.string.lettspeed), int_time.toString());
                mTime = mCustomEditText.getText().toString() + " s";
                mData.set(customRow, int_time.toString());
                return true;
            } catch (NumberFormatException ex) {
                return false;
            }
        }
    }
}