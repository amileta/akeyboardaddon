package com.prediction;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

/**
 * Created by Antonio on 4.6.2015..
 */
public class BigramDataSource extends DataSource {

    //multiplier za predikcije iz korisnickog rijecnika
    private static final int USER_PREDICTION_MULTIPLIER = 2;

    private String[] allColumns = {
            MySQLiteHelper.BIGRAM_ID,
            MySQLiteHelper.BIGRAM_FIRST_ID,
            MySQLiteHelper.BIGRAM_SECOND_ID,
            MySQLiteHelper.BIGRAM_FREQUENCY,
            MySQLiteHelper.BIGRAM_DEFAULT_FREQUENCY
    };
    private String limit;
    //veza sa tablicama unigrama
    private UnigramDataSource ud;

    //konstruktor
    public  BigramDataSource(Context context, UnigramDataSource ud) {
        dbHelper = MySQLiteHelper.getInstance(context);
        mContext = context;
        //todo postavit da se ovo moze dat metodi?
        limit = "10";
        this.ud = ud;
    }


    /**
     * Unosi vezu izmedju dva unigrama u tablicu kao bigram.
     *
     * @param id_first
     * @param id_second
     * @param frequency
     * @return
     */
    public long insertOrUpdateBigram(long id_first, long id_second, int frequency){
        ContentValues values = new ContentValues();
        values.put(MySQLiteHelper.BIGRAM_FIRST_ID, id_first);
        values.put(MySQLiteHelper.BIGRAM_SECOND_ID, id_second);
        values.put(MySQLiteHelper.BIGRAM_FREQUENCY, frequency);
        values.put(MySQLiteHelper.BIGRAM_DEFAULT_FREQUENCY, frequency);

        long insertId = database.insertWithOnConflict(MySQLiteHelper.TABLE_BIGRAMS, null,
                values, SQLiteDatabase.CONFLICT_IGNORE);
        if (insertId > 0) {
            Log.d("SOFT_KEY","Uspjesno unesen novi bigram"+ id_first + ": " + id_second );
            return insertId;
        }
        else {
            String[] projection = {
                    MySQLiteHelper.BIGRAM_ID,
                    MySQLiteHelper.BIGRAM_FIRST_ID,
                    MySQLiteHelper.BIGRAM_SECOND_ID,
                    MySQLiteHelper.BIGRAM_FREQUENCY
            };
            //ako unos nije uspio (bigram vec postoji), potrebno je napraviti update frekvencije postojeceg bigrama, a
            //sacuvati ostale postojece podatke
            Cursor c = database.query(
                    MySQLiteHelper.TABLE_BIGRAMS,  // The table to query
                    projection,                     // The columns to return
                    // The columns for the WHERE clause
                    MySQLiteHelper.BIGRAM_FIRST_ID + "=" + "? AND " + MySQLiteHelper.BIGRAM_SECOND_ID + "=" + "?",
                    new String[]{String.valueOf(id_first), String.valueOf(id_second)},  // The values for the WHERE clause
                    null,                                     // don't group the rows
                    null,
                    null
            );
            if (c.getCount() > 0) {
                c.moveToFirst();
                int id = c.getInt(0);
                int freq = c.getInt(3);
                //close cursor
                c.close();
                //todo ako je frekvencija dostigla predefiniranu granicu potrebno je pokrenuti normalizaciju
                //cijele tablice
                values = new ContentValues();
                //update rijeci
                values.put(MySQLiteHelper.BIGRAM_FREQUENCY, freq + USER_PREDICTION_MULTIPLIER);
                long updateId = database.update(MySQLiteHelper.TABLE_BIGRAMS,  values, MySQLiteHelper.BIGRAM_ID + "=" + "?",
                        new String[]{String.valueOf(id)});
                if (updateId > 0) {
                    Log.d("SOFT_KEY", "Uspjesno unesen postojeci bigram" + updateId + ": ");
                    return updateId;
                }
            }
            //close cursor
            if (!c.isClosed());
            c.close();
            return -1;
        }
    }

    /**
     * Stvara bigram od dva unigrama. Ukoliko dani unigrami ne postoje u tablici unigrama najprije
     * se umeću tamo sa danom frekvencijom.
     *
     * @param first
     * @param second
     * @return
     */
    //zapis bigrama u tablicu
    public Bigram insertOrUpdateBigram(Unigram first, Unigram second, int frequency) {
        ContentValues values = new ContentValues();
        long first_id = first.get_id();
        long second_id = second.get_id();
        if (first_id == 0){
            first = ud.insertOrUpdateUnigram(first);
        }
        if (second_id == 0){
            second = ud.insertOrUpdateUnigram(second);
        }
        Bigram bigram = new Bigram(first, second, frequency);
        long insertId = insertOrUpdateBigram(first.get_id(), second.get_id(), frequency);
        if (insertId > 0) {
            bigram._id = insertId;
            Log.d("SOFT_KEY", "Uspjesno unesen bigram" + bigram._id + ": " + first.getWord()
                    + "," + second.getWord());
            return bigram;
        }
        else {
            //TODO handle ako nije uspio unos
            Log.e("SOFT_KEY","Unos bigrama nije uspio!");
            return null;
        }
    }

    public boolean deleteBigram(long bigram_id) {
        if (database.delete(MySQLiteHelper.TABLE_BIGRAMS, MySQLiteHelper.BIGRAM_ID
                + " = " + bigram_id, null) > 0) {
            System.out.println("Bigram deleted with id: " + bigram_id);
            return true;
        }
        else
            return false;
    }


    public void getMatchingBigramSuggestions(final String previous, SuggestedWords suggestions){
        getMatchingBigramSuggestions(previous, "", suggestions, false);
    }

     /**
     * Dohvaća odgovarajuće bigrame koji slijede dani unigram, sortirane prema vjerojatnosti pojave
     * (normalizirane u rasponu 0 - Dictionary.MAX_FREQUENCY)
     * Ili vraća praznu listu ukoliko nema matcheva.
     *
     * @param previous
     * @param current
     * @param suggestions
     * @param filterByStarting  Postaviti na true za filtriranje prema trenutno upisanom početku riječi
     * @return
     */
    public void getMatchingBigramSuggestionsByProbabilities(final String previous, String current, SuggestedWords suggestions, boolean filterByStarting){
        if (previous == null || previous.length() == 0){
            return;
        }
        //najprije trebamo pronaci id unigrama
        Unigram ug = ud.getUnigram(previous);
        if (ug == null){
            return;
        }
        Log.d(getClass().getSimpleName(), "Filtering by unigram:" + ug.getWord() + ","+ ug.get_id() );
        String QUERY;
        if (!filterByStarting) {
            // How you want the results sorted in the resulting Cursor
            QUERY = "SELECT b." + MySQLiteHelper.BIGRAM_FREQUENCY + ",  u1."
                    + MySQLiteHelper.UNIGRAM_UNIGRAM + " FROM " + MySQLiteHelper.TABLE_BIGRAMS + " b INNER JOIN  "
                    + MySQLiteHelper.TABLE_UNIGRAMS + " u1 ON u1." + MySQLiteHelper.UNIGRAM_ID + " = b."
                    + MySQLiteHelper.BIGRAM_SECOND_ID + " AND b." + MySQLiteHelper.BIGRAM_FIRST_ID + "="
                    + ug.get_id() + " ORDER BY b." + MySQLiteHelper.BIGRAM_FREQUENCY + " DESC LIMIT " + limit;

        } else {
            //filtered by current composing unigram
            QUERY = "SELECT b." + MySQLiteHelper.BIGRAM_FREQUENCY + ",  u1."
                    + MySQLiteHelper.UNIGRAM_UNIGRAM + " FROM " + MySQLiteHelper.TABLE_BIGRAMS + " b INNER JOIN  "
                    + MySQLiteHelper.TABLE_UNIGRAMS + " u1 ON u1." + MySQLiteHelper.UNIGRAM_ID + " = b."
                    + MySQLiteHelper.BIGRAM_SECOND_ID + " AND b." + MySQLiteHelper.BIGRAM_FIRST_ID + "= "
                    + ug.get_id() + " WHERE " + MySQLiteHelper.UNIGRAM_UNIGRAM + " LIKE '" + current+"%' ORDER BY b."
                    + MySQLiteHelper.BIGRAM_FREQUENCY + " DESC LIMIT " + limit;
        }
        Cursor c = database.rawQuery(
                QUERY,  // The table to query
                null);

        while (c.moveToNext()) {
            //Log.d("Aservice", "Adding bigram suggestion by probability:" + c.getString(1));
            //MLE procjena frekvencije => bigram freq (freq rijeci u bigramu) / unigram freq (frekvencija rijeci zasebno)
            float freq = (float) c.getInt(0) / (float) ug.getFrequency();
            int normalized = Math.round(freq * SessionDictionary.BIGRAM_MAX_FREQUENCY);
            Log.d(getClass().getSimpleName(), "Addeed bigram to suggestions: " + c.getString(1) +", score:" + normalized);
            suggestions.add(c.getString(1), normalized);
        }
        //close cursor
        c.close();
    }

    /**
     * Dohvaća odgovarajuće bigrame koji slijede dani unigram, sortirane prema frekvenciji.
     * Ili vraća praznu listu ukoliko nema matcheva.
     *
     * @param previous
     * @param current
     * @param suggestions
     * @param filterByStarting  Postaviti na true za filtriranje prema trenutno upisanom početku riječi
     * @return
     */
    public void getMatchingBigramSuggestions(final String previous, String current, SuggestedWords suggestions, boolean filterByStarting){
        if (previous == null || previous.length() == 0){
            return;
        }
        //najprije trebamo pronaci id unigrama
        int id = ud.getUnigramId(previous);
        if (id == -1){
            return;
        }
        String QUERY;
        if (!filterByStarting) {
            // How you want the results sorted in the resulting Cursor
            QUERY = "SELECT b." + MySQLiteHelper.BIGRAM_FREQUENCY + ",  u1."
                    + MySQLiteHelper.UNIGRAM_UNIGRAM + " FROM " + MySQLiteHelper.TABLE_BIGRAMS + " b INNER JOIN  "
                    + MySQLiteHelper.TABLE_UNIGRAMS + " u1 ON u1." + MySQLiteHelper.UNIGRAM_ID + " = b."
                    + MySQLiteHelper.BIGRAM_SECOND_ID + " AND b." + MySQLiteHelper.BIGRAM_FIRST_ID + "="
                    + id + " ORDER BY b." + MySQLiteHelper.BIGRAM_FREQUENCY + " DESC LIMIT " + limit;

        } else {
            //filtered by current composing unigram
            QUERY = "SELECT b." + MySQLiteHelper.BIGRAM_FREQUENCY + ",  u1."
                    + MySQLiteHelper.UNIGRAM_UNIGRAM + " FROM " + MySQLiteHelper.TABLE_BIGRAMS + " b INNER JOIN  "
                    + MySQLiteHelper.TABLE_UNIGRAMS + " u1 ON u1." + MySQLiteHelper.UNIGRAM_ID + " = b."
                    + MySQLiteHelper.BIGRAM_SECOND_ID + " AND b." + MySQLiteHelper.BIGRAM_FIRST_ID + "= "
                    + id + " WHERE " + MySQLiteHelper.UNIGRAM_UNIGRAM + " LIKE '" + current+"%' ORDER BY b."
                    + MySQLiteHelper.BIGRAM_FREQUENCY + " DESC LIMIT " + limit;
        }
        Cursor c = database.rawQuery(
                QUERY,  // The table to query
                null);
        //todo make it return an array of unigrams for caching results and saving a lookup if it's not too slow
        int i = 0;
        c.moveToFirst();
        while (i < c.getCount()) {
            //Log.d("Aservice", "Adding bigram suggestion:" + c.getString(1));
            suggestions.add(c.getString(1), c.getInt(0));
            i++;
            c.moveToNext();
        }
        //close cursor
        c.close();
    }

    public String getLimit() {
        return limit;
    }

    public void setLimit(String limit) {
        if (Integer.parseInt(limit) <= 0) {
            throw new IllegalArgumentException("Limit mora biti pozitivan broj!");
        }
        this.limit = limit;
    }

    public void fillBigramDatabaseFromFile(){
        new InitialLoadDbTask(mContext, database, ud).execute();
    }

    public int countEntries() {
        String countQuery = "SELECT  * FROM " + MySQLiteHelper.TABLE_BIGRAMS;
        Cursor cursor = database.rawQuery(countQuery, null);
        int cnt = cursor.getCount();
        cursor.close();
        return cnt;
    }

    /**
     * Async task to initially load the bigram database from text file
     */
    private class InitialLoadDbTask extends AsyncTask<Void, Void, Void> {

        private int INPUT_MAXIMUM_FREQUENCY;

        private Context mContext;
        private SQLiteDatabase database;
        private UnigramDataSource ud;

        public InitialLoadDbTask(Context context, SQLiteDatabase db, UnigramDataSource unigram) {
            mContext = context;
            database = db;
            ud = unigram;
        }

        @Override
        protected Void doInBackground(Void... v) {
            if (USE_PRELOADED_DATABASE)
                return null;
            //todo remove ako nadjemo bolje rjesenje npr. SharedPreferences
            if (!dbHelper.tableExists(MySQLiteHelper.TABLE_BIGRAMS, database, false))
                dbHelper.createBigramTable(database);
            else
                return null;
            AssetManager am = mContext.getAssets();
            InputStream inputStream;
            try {
                inputStream = am.open("testbigram.txt");
            } catch (IOException e) {
                Log.e("SQLITE_TEST", "Testdicts not found!");
                return null;
            }
            BufferedReader br;
            //dodajmo iz datoteke
            try {
                br = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
                String line;
                boolean read_max_frequency = false;
                database.beginTransaction();
                //int i=0;
                while ((line = br.readLine()) != null) {
                    //Log.d("SOFT_KEYBOARD", line);
                    String[] stuff = line.split(",");
                    if (!read_max_frequency){
                        INPUT_MAXIMUM_FREQUENCY = Integer.parseInt(stuff[2]);
                        read_max_frequency = true;
                    }
                    long id_first = ud.getUnigramId(stuff[0]);
                    long id_second = ud.getUnigramId(stuff[1]);
                    if (id_first == -1){
                        Unigram unigram1 = new Unigram(stuff[0], 1);
                        unigram1 = ud.insertOrUpdateUnigram(unigram1);
                        id_first = unigram1.get_id();
                    }
                    if (id_second == -1){
                        Unigram unigram2 = new Unigram(stuff[1], 1);
                        unigram2 = ud.insertOrUpdateUnigram(unigram2);
                        id_second = unigram2.get_id();
                    }
                    if (insertOrUpdateBigram(id_first, id_second, normalizeFrequency(Integer.parseInt(stuff[2]))) <= -1){
                        Log.e("SOFT_KEYBOARD", "Bigram insertion failed for bigram:" + stuff[0] + "," + stuff[1]);
                    }
                }
                database.setTransactionSuccessful();
            } catch (UnsupportedEncodingException e) {
                Log.e("SQLITE_TEST", "Encoding not supported!");
                e.printStackTrace();
                return null;
            } catch (IOException e) {
                Log.e("SQLITE_TEST", "Bigram loading failed!");
                e.printStackTrace();
            } finally {
                database.endTransaction();
            }
            return null;


        }

        /**
         * Funkcija za normalizaciju frekvencija riječnika.
         * Rezultat normalkizacije ovisi, među ostalim i o maksimalnoj frekvenciji
         * koju je moguce pohraniti u rijecniku kako je definirano u SessionDictionary.
         * Algoritam normalizacije je sljedeci:
         *  - promatramo riječnik kao da jedan veliki dokument te se koristi
         *      maximum tf normalizacija, dakle ovisno o najčešćoj riječi u riječniku (koja mora biti 1.
         *      u ulaznom formatu)
         *  - dobivena relativna frekvencija se množi sa pola maksimalno moguće u riječniku pohrane
         *
         *  Ovime se dobiju najmanje početne frekvencije oko 6, što znači da će korisnik morati 6 puta
         *  iskoristiti neku svoju novu riječ da ona postane vrijedna kao i početno unesene riječi
         *
         * @param freq
         * @return
         */
        private int normalizeFrequency(final int freq){
            float normalized = 0.05f + (0.95f * ((float) freq / (float)INPUT_MAXIMUM_FREQUENCY));
            return Math.round(normalized * (SessionDictionary.BIGRAM_MAX_FREQUENCY/2));
        }
    }
}
