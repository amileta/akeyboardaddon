package com.prediction;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Stemmer {
    static String stop[]={"biti","jesam","budem","sam","jesi","budeš","si","jesmo","budemo","smo","jeste","budete","ste","jesu","budu","su","bih","bijah","bjeh","bijaše","bi","bje","bješe","bijasmo","bismo","bjesmo","bijaste","biste","bjeste","bijahu","biste","bjeste","bijahu","bi","biše","bjehu","bješe","bio","bili","budimo","budite","bila","bilo","bile","ću","ćeš","će","ćemo","ćete","želim","želiš","želi","želimo","želite","žele","moram","moraš","mora","moramo","morate","moraju","trebam","trebaš","treba","trebamo","trebate","trebaju","mogu","možeš","može","možemo","možete"};
    String[] nastavak = new String[102];
    String[] osnova = new String[102];
    String[] transformacije = new String[131];
    public Stemmer(StemmerRulesProvider rulesProvider) throws IOException {
        BufferedReader rules= new BufferedReader(new InputStreamReader(rulesProvider.getRulesInputStream(), "UTF-8"));
        String everything;
        BufferedReader trans= new BufferedReader(new InputStreamReader(rulesProvider.getTransformationsInputStream(), "UTF-8"));
        String everything2;
        try {
            StringBuilder sb = new StringBuilder();
            String line = rules.readLine();

            while (line != null) {
                sb.append(line);
                sb.append(System.getProperty("line.separator"));
                line = rules.readLine();
            }
            everything = sb.toString();
        } finally {
            rules.close();
        }
        try {
            StringBuilder sb = new StringBuilder();
            String line = trans.readLine();

            while (line != null) {
                sb.append(line);
                sb.append(System.getProperty("line.separator"));
                line = trans.readLine();
            }
            everything2 = sb.toString();
        } finally {
            trans.close();
        }
        String[] pravila= everything.split("\\n");
        String[] temp=null;
        for(int i=0;i<pravila.length;i++){
            temp=pravila[i].split("\\s");

            this.osnova[i]=temp[0];
            this.nastavak[i]=temp[1];

        }
        this.transformacije= everything2.split("\\s");
    }


    public static String istakniSlogotvornoR(String niz){
        return  niz.replaceAll("(^|[^aeiou])r($|[^aeiou])","$1R$2");}

    public static boolean imaSamoglasnik(String niz){
        Pattern pattern = Pattern.compile("[aeiouR]");
        Matcher matcher = pattern.matcher(niz);
        boolean matches = matcher.matches();

        if(matches)
            return false;
        return true;}

    /**
     * Parametar pojavnica bi trebao biti lowercase?
     *
     * @param pojavnica
     * @return
     */
    public String transformiraj(String pojavnica){
        String trazi=new String();

        for (int i=0;i<this.transformacije.length-1;i=i+3){
            trazi=this.transformacije[i];
            String zamjeni=this.transformacije[i+1];
            if (pojavnica.endsWith(trazi)){
                pojavnica=pojavnica.substring(0,pojavnica.length()-trazi.length());
                pojavnica=pojavnica+zamjeni;
                return pojavnica;}}

        return pojavnica;
    }


    /**
     * Koristi se u kombinaciji sa transformiraj
     *
     * @param pojavnica
     * @return
     */
    public String korjenuj(String pojavnica){

        for (int i=0;i<this.nastavak.length;i++){

            Pattern pattern = Pattern.compile("^("+this.osnova[i]+")("+this.nastavak[i]+")$");
            //System.out.println(this.osnova[i]+" "+this.nastavak[i]);
            Matcher matcher = pattern.matcher(pojavnica);
            boolean matches = matcher.matches();

            if(matches){
                //System.out.println(matcher.group(1));
                if ((matcher.group(1)!= null)&& (imaSamoglasnik(matcher.group(1))&&(matcher.group(1).length()>1))){

                    return matcher.group(1);}}}
        return pojavnica;}
}
