package com.prediction;

import android.content.Context;
import android.content.res.AssetManager;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Antonio on 11.7.2015..
 *
 * Pomoćna klasa koja dohvaća datoteke sa pravilima za Stemmer
 *
 */
public class StemmerRulesProvider {

    InputStream rulesStream;
    InputStream transfromationsStream;

    public StemmerRulesProvider(Context context) throws IOException {
        AssetManager am = context.getAssets();
        rulesStream = am.open("rules.txt");
        transfromationsStream = am.open("transformations.txt");

    }

    public InputStream getRulesInputStream(){
        return rulesStream;
    }

    public InputStream getTransformationsInputStream(){
        return transfromationsStream;
    }
}
