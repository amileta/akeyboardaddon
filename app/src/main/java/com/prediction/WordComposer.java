package com.prediction;

/*
 * Copyright (C) 2008-2009 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Antonio on 13.5.2015..
 *
 * Klasa za pohranu rijeci koju trenutno pisemo.
 */
public class WordComposer {
    /**
     * The list of unicode values for each keystroke (including surrounding keys, as an array of ints)
     */
    private List<int[]> mCodes;

    /**
     * The word chosen from the candidate list, until it is committed.
     */
    private String mPreferredWord;

    private StringBuilder mTypedWord;

    /**
     * Whether the user chose to capitalize the word.
     */
    private boolean mIsCapitalized;

    /** Constructor */
    public WordComposer() {
        mCodes = new ArrayList<int[]>(12);
        mTypedWord = new StringBuilder(20);
    }

    /**
     * Clear out the keys registered so far.
     */
    public void reset() {
        mCodes.clear();
        mIsCapitalized = false;
        mPreferredWord = null;
        mTypedWord.setLength(0);
    }

    /** Number of keystrokes in the composing word.
    * @return the number of keystrokes
    */
    public int size() {
        return mCodes.size();
    }

    /**
     * Returns the codes at a particular position in the word.
     * @param index the position in the word
     * @return the unicode for the pressed and surrounding keys
     */
    public int[] getCodesAt(int index) {
        return mCodes.get(index);
    }

    /**
     * Add a new_prediction keystroke, with codes[0] containing the pressed key's unicode and the rest of
     * the array containing unicode for adjacent keys, sorted by reducing probability/proximity.
     * @param codes the array of unicode values
     */
    public void add(int primaryCode, int[] codes) {
        mTypedWord.append((char) primaryCode);
        mCodes.add(codes);
    }

    /**
     * Delete the last keystroke as a result of hitting backspace.
     */
    public void deleteLast() {
        mCodes.remove(mCodes.size() - 1);
        mTypedWord.deleteCharAt(mTypedWord.length() - 1);
    }

    public String getTypedWord() {
        int wordSize = mCodes.size();
        if (wordSize == 0) {
            return null;
        }
        return mTypedWord.toString();
    }

    public void setCapitalized(boolean capitalized) {
        mIsCapitalized = capitalized;
    }


    /**
     * Whether or not the user typed a capital letter as the first letter in the word
     * @return capitalization preference
     */
    public boolean isCapitalized() {
        return mIsCapitalized;
    }

    /**
     * Stores the user's selected word, before it is actually committed to the text field.
     * @param preferred
     */
    public void setPreferredWord(String preferred) {
        mPreferredWord = preferred;
    }

    /**
     * Return the word chosen by the user, or the typed word if no other word was chosen.
     * @return the preferred word
     */
    public String getPreferredWord() {
        return mPreferredWord != null ? mPreferredWord : getTypedWord();
    }

    /**
     * TODO add auto capitalize options
     * Returns whether the word was automatically capitalized.
     * @return whether the word was automatically capitalized
     */
    public boolean isAutoCapitalized() {
        return mIsCapitalized;
    }

    public void setCurrentWord(String currentWord) {
        for (int i = 0; i < currentWord.length(); i++){
            add(currentWord.charAt(i), new int[]{currentWord.charAt(i)});
        }
    }
}
