package com.prediction;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Antonio on 5.6.2015..
 */
public class DataSource {

    //kontrolira ako koristimo preloaded bazu podatka
    protected static boolean USE_PRELOADED_DATABASE = true;

    // Database fields
    protected static SQLiteDatabase database;
    protected MySQLiteHelper dbHelper;
    protected Context mContext;

    public boolean open() throws SQLException {
        if (dbHelper.isUpdating())
            return false;
        database = dbHelper.getWritableDatabase();
        return true;
    }

    public void close() {
        dbHelper.close();
    }

    public void beginBulkOperation(){
        database.beginTransaction();
    }

    public void endBulkOperation(boolean success){
        if (success){
            database.setTransactionSuccessful();
            database.endTransaction();
        }
    }

    /**
     * Dohvaća frekvenciju riječi. Default implementacija vraća -1, preraditi sa vlastitom.
     *
     * @param word
     * @return
     */
    public int getFrequency(CharSequence word) {
        return -1;
    }
}
