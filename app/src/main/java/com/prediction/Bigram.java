package com.prediction;


/**
 * Created by Antonio on 4.6.2015..
 */
public class Bigram {
    long _id;
    Unigram first;
    Unigram second;
    int frequency;

    public Bigram(Unigram first, Unigram second, int frequency){
        this.first = first;
        this.second = second;
        this.frequency = frequency;
    }

    public Bigram(long _id, Unigram first, Unigram second, int frequency){
        this(first, second, frequency);
        this._id = _id;
    }
}
