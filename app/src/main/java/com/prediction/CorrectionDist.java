package com.prediction;

import java.util.ArrayList;


public class CorrectionDist {
	//za test
		static String[] baza={"kuća","majica","majice","stan","query","mama"};

    //izvor podataka za rijeci
    private Unigram[] words;
    private SuggestedWords suggestions;

    public CorrectionDist(){

    }

    /**
     * Prima klasu koja pohranjuje sve prijedloge
     *
     * @param suggestions
     */
    public CorrectionDist(SuggestedWords suggestions){
        this.suggestions = suggestions;
    }

    /**
     * Izračunava Levensthein udaljenost izmedju dvije rijeci.
     *
     * TODO make it FASTER!
     *
     * @param word
     * @param word2
     * @return
     */
	public int LevenshteinDistance(String word,String word2){
		  int cost=0;
		  if (word.length() == 0) return word2.length();
		  if (word2.length() == 0) return word.length();

		  /* test if last characters of the strings match */
		  if (word.substring(0,word.length()-1).equals(word2.substring(0,word2.length()-1)))
		      cost = 0;
		  else
		      cost = 1;

		  /* return minimum of delete char from s, delete char from t, and delete char from both */
		  return Math.min(LevenshteinDistance(word.substring(0,word.length()-1),word2) + 1,
		                Math.min( LevenshteinDistance(word,word2.substring(0,word2.length()-1)) + 1,
		                 LevenshteinDistance(word.substring(0,word.length()-1),word2.substring(0,word2.length()-1)) + cost));
		
		
		
	}

    /**
     * Dodaje u spremiste trenutnih predikcija sve predikcije temeljene na udaljenosti
     *
     * @param word
     */
    public void AddDistPredictions(String word){
        //ArrayList<String> suggestion=new ArrayList();
        if (words == null){
            return;
        }
        for(int i=0;i<this.words.length;i++){
            int dist = LevenshteinDistance(word,words[i].getWord());
            if(dist<=2)
                suggestions.add(words[i].getWord(), words[i].getFrequency());
            //System.out.println(dist+ "|" +words[i].getWord());
        }
    }

    /**
     * Vraca predikcije na temelju udaljenosti
     * TODO napravit da koristi bazu, a ne test polje
     * @param word
     * @return
     */
	public ArrayList<String> GiveDistPrediction(String word){
		ArrayList<String> suggestion=new ArrayList();
		for(int i=0;i<this.baza.length;i++){
			if(LevenshteinDistance(word,baza[i])<=2)
			suggestion.add(baza[i]);
			System.out.println(LevenshteinDistance(word,baza[i])+baza[i]);
		}
		
		
		return suggestion;
	}

	public int OptimalStringAlignmentDistance(String str1, String str2) {
        // d is a table with lenStr1+1 rows and lenStr2+1 columns
        int d[][] = new int[str1.length()][str2.length()];

        // i and j are used to iterate over str1 and str2
        int i, j, cost;

        // for loop is inclusive, need table 1 row/column larger than string length
        for (i = 0; i < str1.length() - 1; i++)
            d[i][0] = i;
        for (j = 0; j < str2.length() - 1; j++) {
            d[0][j] = j;
        }

        // pseudo-code assumes string indices start at 1, not 0
        // if implemented, make sure to start comparing at 1st letter of strings
        for (i = 1; i < str1.length() - 1; i++) {
            for (j = 1; j < str2.length() - 1; j++) {

                if (str1.charAt(i) == str2.charAt(j))
                    cost = 0;
                else cost = 1;

                d[i][j] = Math.min(
                        Math.min(d[i - 1][j] + 1,     // deletion
                                d[i][j - 1] + 1),    // insertion
                        d[i - 1][j - 1] + cost   // substitution
                );
                if (i > 1 && j > 1 && str1.charAt(i) == str2.charAt(j - 1) && str1.charAt(i - 1) == str2.charAt(j))
                    d[i][j] = Math.min(
                            d[i][j],
                            d[i - 2][j - 2] + cost   // transposition
                    );
            }
        }
        return d[str1.length()-2][str2.length()-2];
    }

    /**
     * Setter za izvor svih mogućih riječi za predikciju, potrebno postaviti prije korištenja
     * todo napravit nesto bolje?
     * @param words
     */
    public void setWords(Unigram[] words){
        this.words = words;
    }

}
