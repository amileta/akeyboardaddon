package com.prediction;

/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

import android.content.Context;
import android.os.AsyncTask;
import android.provider.BaseColumns;

import java.util.HashSet;
import java.util.List;

/**
 * Created by Antonio on 13.5.2015..
 *
 * Rijecnik za frekvencije bigrama (parova rijeci)
 */
public class SessionBigramDictionary extends SessionDictionary implements Dictionary.WordCallback {

    public AsyncTask writeTask;

    /** Any pair being typed or picked */
    private static final int FREQUENCY_FOR_TYPED = 2;

    /** Maximum frequency for all pairs */
    private static final int FREQUENCY_MAX = 127;

    /** Maximum number of pairs. Pruning will start when databases goes above this number. */
    private static int sMaxUserBigrams = 10000;

    /**
     * When it hits maximum bigram pair, it will delete until you are left with
     * only (sMaxUserBigrams - sDeleteUserBigrams) pairs.
     * Do not keep this number small to avoid deleting too often.
     */
    private static int sDeleteUserBigrams = 2000;

    /**
     * Database version should increase if the database structure changes
     */
    private static final int DATABASE_VERSION = 1;

    private static final String DATABASE_NAME = "bigram.db";

    /** Name of the words table in the database */
    private static final String MAIN_TABLE_NAME = "main";

    private static final String MAIN_COLUMN_ID = BaseColumns._ID;
    private static final String MAIN_COLUMN_WORD1 = "word1";
    private static final String MAIN_COLUMN_WORD2 = "word2";
    private static final String MAIN_COLUMN_LOCALE = "locale";

    /** Name of the frequency table in the database */
    private static final String FREQ_TABLE_NAME = "frequency";
    private static final String FREQ_COLUMN_ID = BaseColumns._ID;
    private static final String FREQ_COLUMN_PAIR_ID = "pair_id";
    private static final String FREQ_COLUMN_FREQUENCY = "freq";

    /** Locale for which this auto dictionary is storing words */
    private String mLocale;

    private HashSet<LightBigram> mPendingWrites = new HashSet<LightBigram>();
    private final Object mPendingWritesLock = new Object();
    private static volatile boolean sUpdatingDB = false;
    private BigramDataSource mBigramDataSource;
    private SuggestedWords mSuggestedWords;

    /**
     * Lightweight Bigram klasa za pohranu bigrama u trenutnoj sesiji
     */
    private static class LightBigram {
        public final String mWord1;
        public final String mWord2;
        public final int mFrequency;
        LightBigram(String word1, String word2, int frequency) {
            this.mWord1 = word1;
            this.mWord2 = word2;
            this.mFrequency = frequency;
        }
        @Override
        public boolean equals(Object bigram) {
            LightBigram bigram2 = (LightBigram) bigram;
            return (mWord1.equals(bigram2.mWord1) && mWord2.equals(bigram2.mWord2));
        }
        @Override
        public int hashCode() {
            return (mWord1 + " " + mWord2).hashCode();
        }
    }

    //todo delete?
    public void setDatabaseMax(int maxUserBigram) {
        sMaxUserBigrams = maxUserBigram;
    }

    //todo delete?
    public void setDatabaseDelete(int deleteUserBigram) {
        sDeleteUserBigrams = deleteUserBigram;
    }

    /** konstruktor **/
    public SessionBigramDictionary(Context context, BigramDataSource bs, String locale, int dicTypeId, SuggestedWords suggestedWords) {
        super(context, dicTypeId);
        mLocale = locale;
        mBigramDataSource = bs;
        mSuggestedWords = suggestedWords;
    }

    /**
     * Pair will be added to the userbigram database.
     */
    public int addBigrams(String word1, String word2) {
        //remove caps [if second word is autocapitalized] ALWAYS
        word2 = Character.toLowerCase(word2.charAt(0)) + word2.substring(1);
        // Do not insert a word as a bigram of itself
        if (word1.equals(word2)) {
            return 0;
        }
        int freq = addBigram(word1, word2, FREQUENCY_FOR_TYPED);
        if (freq > FREQUENCY_MAX) freq = FREQUENCY_MAX;
        synchronized (mPendingWritesLock) {
            if (freq == FREQUENCY_FOR_TYPED || mPendingWrites.isEmpty()) {
                mPendingWrites.add(new LightBigram(word1, word2, freq));
            } else {
                LightBigram bi = new LightBigram(word1, word2, freq);
                mPendingWrites.remove(bi);
                mPendingWrites.add(bi);
            }
        }
        return freq;
    }

    /**
     * Schedules a background thread to write any pending words to the database.
     */
    public void flushPendingWrites() {
        synchronized (mPendingWritesLock) {
            // Nothing pending? Return
            if (mPendingWrites.isEmpty()) return;
            // Create a background thread to write the pending entries
            writeTask = new UpdateDbTask(getContext(), mBigramDataSource, mPendingWrites, mLocale).execute();
            // Create a new_prediction map for writing new_prediction entries into while the old one is written to db
            mPendingWrites = new HashSet<LightBigram>();
        }
    }

    /**
     * Async task to write pending words to the database so that it stays in sync with
     * the in-memory trie.
     */
    private static class UpdateDbTask extends AsyncTask<Void, Void, Void> {
        private final HashSet<LightBigram> mSet;
        private final BigramDataSource mDataSource;
        private final String mLocale;
        private int errors = 0;

        public UpdateDbTask(@SuppressWarnings("unused") Context context, BigramDataSource bs, HashSet<LightBigram> pendingWrites, String locale) {
            mDataSource = bs;
            mSet = pendingWrites;
            mLocale = locale;
        }
        @Override
        protected Void doInBackground(Void... v) {
            mDataSource.beginBulkOperation();
                for (LightBigram bigram : mSet) {
                    //todo skuzit zasto dodje do nulla
                    if (bigram.mWord1 == null || bigram.mWord2 == null)
                        continue;
                    Bigram inserted = mDataSource.insertOrUpdateBigram(new Unigram(bigram.mWord1, bigram.mFrequency), new Unigram(bigram.mWord2, bigram.mFrequency), bigram.mFrequency);
                    if (inserted == null)
                        errors++;
                }
                if (errors == 0) {
                    mDataSource.endBulkOperation(true);
                } else {
                    mDataSource.endBulkOperation(false);
                }
                mSet.clear();
                return null;
        }
    }


    @Override
    public void close() {
        flushPendingWrites();
        // Don't close the database as locale changes will require it to be reopened anyway
        // Also, the database is written to somewhat frequently, so it needs to be kept alive
        // throughout the life of the process.
        // mOpenHelper.close();
    }

    @Override
    public void getWords(List<int[]> composer, WordCallback callback, ProximityInfo proximityInfo) {

    }

    @Override
    public int isValidWord(String word) {
        return -1;
    }

    /**
     * Dohvaća prijedloge bigrama iz sesije na temelju prethodne riječi
     *
     * @param previous
     */
    public void getSuggestions(String previous){
        if (previous != null && !previous.isEmpty())
            runBigramReverseLookUp(previous, null, this);
    }

    /**
     * Dohvaća prijedloge bigrama iz sesije na temelju prethdone riječi filtrirane prema trenutnoj riječi
     * @param previous
     * @param current
     */
    public void getSuggestionsFiltered(String previous, String current){
        if (previous != null && !previous.isEmpty())
            runBigramReverseLookUp(previous, current, this);
    }

    @Override
    public boolean addWord(final char[] word, int wordOffset, int wordLength, int score, int dicTypeId, Dictionary.DataType dataType) {
        String wordToAdd = new String(word,wordOffset,wordLength);
        //todo smislit kako rangirat bigrame iz sessiona
        //int oldscore = mSuggestedWords.getScore(wordToAdd);
        mSuggestedWords.add(wordToAdd, score);
        return true;
    }
}