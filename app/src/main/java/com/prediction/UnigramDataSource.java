package com.prediction;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

/**
 * Created by Antonio on 4.6.2015..
 */
public class UnigramDataSource extends DataSource {

    //težina kojom se množe frekvencije pri interpolaciji vjerojatnosti
    public static float INTERPOLATION_WEIGHT = 0.4f;

    //multiplier za predikcije iz korisnickog rijecnika, kontrolira brzinu prilagodbe u odnosu
    //na default predikciju
    private static final int USER_PREDICTION_MULTIPLIER = 0;

    private String[] allColumns = { MySQLiteHelper.UNIGRAM_ID,
            MySQLiteHelper.UNIGRAM_UNIGRAM, MySQLiteHelper.UNIGRAM_FREQUENCY,
            MySQLiteHelper.UNIGRAM_INAPPROPRIATE, MySQLiteHelper.UNIGRAM_TYPE,
            MySQLiteHelper.UNIGRAM_ROOT, MySQLiteHelper.UNIGRAM_DEFAULT_FREQUENCY};

    private String limit;


    public String getLimit() {
        return limit;
    }

    public void setLimit(String limit) {
        if (Integer.parseInt(limit) <= 0){
            throw new IllegalArgumentException("Limit mora biti pozitivan broj!");
        }
        this.limit = limit;
    }

    //konstruktor
    public UnigramDataSource(Context context) {
        dbHelper = MySQLiteHelper.getInstance(context);
        mContext = context;
        limit = "10";
    }

    //zapis unigrama u tablicu
    public Unigram insertOrUpdateUnigram(Unigram unigram) {
        ContentValues values = new ContentValues();
        values.put(MySQLiteHelper.UNIGRAM_UNIGRAM, unigram.word);
        values.put(MySQLiteHelper.UNIGRAM_FREQUENCY, unigram.frequency);
        values.put(MySQLiteHelper.UNIGRAM_INAPPROPRIATE, unigram.inappropriate);
        values.put(MySQLiteHelper.UNIGRAM_TYPE, unigram.type);
        values.put(MySQLiteHelper.UNIGRAM_ROOT, unigram.root);
        values.put(MySQLiteHelper.UNIGRAM_DEFAULT_FREQUENCY, 0); //default freq new words always 0
        values.put(MySQLiteHelper.UNIGRAM_LENGTH, unigram.getWord().length());

        long insertId = database.insertWithOnConflict(MySQLiteHelper.TABLE_UNIGRAMS, null,
                values, SQLiteDatabase.CONFLICT_IGNORE);
        if (insertId > 0) {
            unigram._id = insertId;
            return unigram;
        }
        else {
            String[] projection = {
                    MySQLiteHelper.UNIGRAM_ID,
                    MySQLiteHelper.UNIGRAM_UNIGRAM,
                    MySQLiteHelper.UNIGRAM_FREQUENCY
            };
            //ako unos nije uspio (rijec vec postoji), potrebno je napraviti update frekvencije postojece rijeci, a
            //sacuvati ostale postojece podatke
            Cursor c = database.query(
                    MySQLiteHelper.TABLE_UNIGRAMS,  // The table to query
                    projection,                     // The columns to return
                    // The columns for the WHERE clause
                    MySQLiteHelper.UNIGRAM_UNIGRAM + "= ?", new String[]{unigram.getWord()},  // The values for the WHERE clause
                    null,                                     // don't group the rows
                    null,
                    null
            );
            if (c.getCount() > 0) {
                c.moveToFirst();
                int freq = c.getInt(2);
                unigram.set_id(c.getInt(0));
                unigram.setFrequency(freq + USER_PREDICTION_MULTIPLIER);
                //close cursor
                c.close();
                //todo ako je frekvencija dostigla predefiniranu granicu potrebno je pokrenuti normalizaciju
                //cijele tablice
                values = new ContentValues();
                //update rijeci
                values.put(MySQLiteHelper.UNIGRAM_FREQUENCY, freq + USER_PREDICTION_MULTIPLIER);
                long updateId = database.update(MySQLiteHelper.TABLE_UNIGRAMS,  values, MySQLiteHelper.UNIGRAM_ID + "= ?",
                        new String[]{String.valueOf(unigram.get_id())});
                if (updateId > 0) {
                    //Log.d("SOFT_KEY", "Uspjesno unesena rijec" + unigram.get_id() + ": " + unigram.getWord());
                    return unigram;
                }
            }
            //close cursor
            if (!c.isClosed());
                    c.close();
            return null;
        }
    }

    public boolean deleteUnigram(long unigram_id) {
        if (database.delete(MySQLiteHelper.TABLE_UNIGRAMS, MySQLiteHelper.UNIGRAM_ID
                + " = " + unigram_id, null) > 0) {
            System.out.println("Unigram deleted with id: " + unigram_id);
            return true;
        }
        else
            return false;
    }

    /**
     * Dohvaća prijedloge unigrama koji odgovaraju danom prefixu.
     * Bodovi prijedloga se opcionalno mogu utežiti parametrom weight koji mora biti manji od 1.
     * (korisno kod interpolacije vjerojatnosti tj. kombinacije podataka bigrama i unigrama)
     *
     * @param prefix
     * @param suggestions
     * @param weight
     */
    public void getMatchingUnigramSuggestions(final String prefix, SuggestedWords suggestions, float weight){
        if (weight <= 0 || weight > 1){
            throw new IllegalArgumentException("Težina mora biti u intervalu između 0 i 1!");
        }
        Unigram[] unigrams = getMatchingUnigrams(prefix);
        for (Unigram unigram : unigrams){
            suggestions.add(unigram.getWord(), (int)Math.ceil(weight * unigram.getFrequency()));
        }
    }

    public void getMatchingUnigramSuggestions(final String prefix, SuggestedWords suggestions) {
        getMatchingUnigramSuggestions(prefix, suggestions, 1);
    }

    public void getMatchingUnigramSuggestions(final String prefix, SuggestedWords suggestions, boolean inapproprate){
        Unigram[] unigrams = getMatchingUnigrams(prefix);
        for (Unigram unigram : unigrams){
            suggestions.add(unigram.getWord(), unigram.getFrequency());
        }
    }

    public Unigram[] getMatchingUnigramsUnfiltered(String prefix) {
        String[] projection = {
                MySQLiteHelper.UNIGRAM_ID,
                MySQLiteHelper.UNIGRAM_UNIGRAM,
                MySQLiteHelper.UNIGRAM_FREQUENCY,
                MySQLiteHelper.UNIGRAM_INAPPROPRIATE,
                MySQLiteHelper.UNIGRAM_TYPE,
                MySQLiteHelper.UNIGRAM_ROOT
        };

        // How you want the results sorted in the resulting Cursor
        String sortOrder = MySQLiteHelper.UNIGRAM_FREQUENCY + " DESC,"
                + MySQLiteHelper.UNIGRAM_LENGTH + " ASC";
        Cursor c = database.query(
                MySQLiteHelper.TABLE_UNIGRAMS,  // The table to query
                projection,                     // The columns to return
                // The columns for the WHERE clause
                MySQLiteHelper.UNIGRAM_UNIGRAM + " LIKE ?", new String[]{prefix+"%"},  // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder,
                limit                // The sort order
        );
        Unigram[] words = new Unigram[c.getCount()];
        int i = 0;
        c.moveToFirst();
        while (i < c.getCount()) {
            words[i] =  cursorToUnigram(c);
            i++;
            c.moveToNext();
        }
        //close cursor
        c.close();
        return words;
    }

    /**
     * Dohvaća sve oblike danog unigrama. Ukoliko je ulazni parametar null vraća null.
     *
     * @param root
     * @return
     */
    public Unigram[] getUnigramFormsFromRoot(String root){
        if (root == null){
            throw  new IllegalArgumentException("Korijen ne smije biti null!");
        }
        String[] projection = {
                MySQLiteHelper.UNIGRAM_ID,
                MySQLiteHelper.UNIGRAM_UNIGRAM,
                MySQLiteHelper.UNIGRAM_FREQUENCY,
                MySQLiteHelper.UNIGRAM_INAPPROPRIATE,
                MySQLiteHelper.UNIGRAM_TYPE,
                MySQLiteHelper.UNIGRAM_ROOT
        };

        String sortOrder = MySQLiteHelper.UNIGRAM_FREQUENCY + " DESC,"
                + MySQLiteHelper.UNIGRAM_LENGTH + " DESC";
        Cursor c = database.query(
                MySQLiteHelper.TABLE_UNIGRAMS,  // The table to query
                projection,                     // The columns to return
                // The columns for the WHERE clause
                MySQLiteHelper.UNIGRAM_ROOT + "=?", new String[]{root},
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder,
                limit                // The sort order

        );
        Unigram[] words = new Unigram[c.getCount()];
        int i = 0;
        c.moveToFirst();
        while (i < c.getCount()) {
            words[i] =  cursorToUnigram(c);
            i++;
            c.moveToNext();
        }

        //close cursor
        c.close();
        return words;
    }

    /**
     * Dohvaća sve oblike danog unigrama.
     *
     * @param unigram
     * @param suggestedWords
     * @return
     */
    public void getUnigramForms(String unigram, SuggestedWords suggestedWords){
        String root = getUnigramRoot(unigram);
        if (root != null && !root.isEmpty()){
           Unigram[] forms = getUnigramFormsFromRoot(root);
            if (forms != null) {
                for (Unigram form : forms) {
                    suggestedWords.add(form.getWord(), form.getFrequency());
                }
            }
        }
    }

    //todo maknut ovo u BigramDataSource pošto koristi njegove tablice?
    public void getUnigramFormsByBigram(String previous, String word, SuggestedWords suggestions) {
        //dohvati osnovni oblik
        String root = getUnigramRoot(word);
        if (root == null || root.isEmpty()){
            //ako nije nadjen znači da riječ ne postoji u bazi pa samo dodaj nju sa osnovnom frekvencijom 1
            suggestions.add(word, 1);
            return;
        }
        //dohvati sve oblike na temelju osnovnog
        Unigram[] forms = getUnigramFormsFromRoot(root);
        String inString = "";
        for (int i = 0; i < forms.length; i++){
            //Log.d("SOFT_KEY", "Adding form:" + forms[i].getWord() + "," + forms[i].getFrequency());
            suggestions.add(forms[i].getWord(), (int)Math.ceil(forms[i].getFrequency() * INTERPOLATION_WEIGHT));
            if (i!=forms.length-1)
                inString += "'" + forms[i].getWord() + "',";
            else
                inString += "'" + forms[i].getWord() + "'";
        }
        //dohvati id prethodne rijeci bigrama, ako postoji
        int bigram_id = -1;
        if (previous != null && !previous.isEmpty())
            bigram_id = getUnigramId(previous);
        //ako ne postoji nema smisla traziti dalje
        if (bigram_id == -1) {
            return;
        }
        //ako postoji dohvati odgovarajuce bigrame i njihove frekvencije
        String QUERY = "SELECT b." + MySQLiteHelper.BIGRAM_FREQUENCY + ",  u1."
                + MySQLiteHelper.UNIGRAM_UNIGRAM + " FROM " + MySQLiteHelper.TABLE_BIGRAMS + " b INNER JOIN  "
                + MySQLiteHelper.TABLE_UNIGRAMS + " u1 ON u1." + MySQLiteHelper.UNIGRAM_ID + " = b."
                + MySQLiteHelper.BIGRAM_SECOND_ID + " AND " + MySQLiteHelper.BIGRAM_FIRST_ID + " = "
                + bigram_id + " WHERE " + MySQLiteHelper.UNIGRAM_UNIGRAM + " IN (" + inString + ") ORDER BY b."
                + MySQLiteHelper.BIGRAM_FREQUENCY + " DESC LIMIT " + limit;
        Cursor c = database.rawQuery(
                QUERY,  // The table to query
                null);
        //todo make it return an array of unigrams for caching results and saving a lookup if it's not too slow
        int i = 0;
        c.moveToFirst();
        while (i < c.getCount()) {
            //Log.d("SOFT_KEY", "Adding unigram ordered by bigram likelihood suggestion:" + c.getString(1));
            //provjerimo ako je oblik već upisan i ako je povećajmo mu score
            //praktički povećava score svim bigramima da dođu prije unigrama ?
            int score = suggestions.getScore(c.getString(1));
            //todo testirat je li potreban bolji multiplier za pravilan poredak prijedloga - ovaj racuna vjerojatnost pojave i normalizira u rasponu do BIGRAM MAX FREQUENCY
            score = score == -1 ? c.getInt(0) : (int) Math.ceil(((float)c.getInt(0) / score)  * SessionDictionary.BIGRAM_MAX_FREQUENCY);
            //Log.d("SOFT_KEY", "Changing frequency of form:" + c.getString(1) + "to" + score);
            suggestions.add(c.getString(1), score);
            i++;
            c.moveToNext();
        }
        //close cursor
        c.close();
    }

    /**
     * Dohvaća osnovni oblik iz baze na temelju danog unigrama
     * Vraća null ukoliko ne postoji u bazi.
     *
     * @param unigram
     * @return
     */
    private String getUnigramRoot(String unigram){
        String root = null;
        String[] root_projection = {
                MySQLiteHelper.UNIGRAM_ID,
                MySQLiteHelper.UNIGRAM_ROOT
        };

        //stavio query COLLATE NOCASE da bi dobili bez obzira na case
        Cursor c = database.query(
                MySQLiteHelper.TABLE_UNIGRAMS,  // The table to query
                root_projection,                     // The columns to return
                // The columns for the WHERE clause
                MySQLiteHelper.UNIGRAM_UNIGRAM + " = ?", new String[]{unigram},
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null,
                limit                // The sort order
        );
        if (c.getCount() > 0) {
            c.moveToFirst();
            root = c.getString(c.getColumnIndex(MySQLiteHelper.UNIGRAM_ROOT));
            c.close();
        }
        return root;
    }

    /**
     *
     * Dohvaća sve unigrame koji odgovaraju prefiksu.
     *
     * @param prefix
     * @return
     */
    public Unigram[] getMatchingUnigrams(String prefix){
        String[] projection = {
                MySQLiteHelper.UNIGRAM_ID,
                MySQLiteHelper.UNIGRAM_UNIGRAM,
                MySQLiteHelper.UNIGRAM_FREQUENCY,
                MySQLiteHelper.UNIGRAM_INAPPROPRIATE,
                MySQLiteHelper.UNIGRAM_TYPE,
                MySQLiteHelper.UNIGRAM_ROOT
        };

        //TODO prilagodit query za podršku različitih jezika (ovo ĐĐĐĐ je prilagođeno za hrv. znakove)
        // How you want the results sorted in the resulting Cursor
        String sortOrder = MySQLiteHelper.UNIGRAM_FREQUENCY + " DESC,"
        + MySQLiteHelper.UNIGRAM_LENGTH + " ASC";
        Cursor c = database.query(
                MySQLiteHelper.TABLE_UNIGRAMS,  // The table to query
                projection,                     // The columns to return
                // The columns for the WHERE clause
                //MySQLiteHelper.UNIGRAM_UNIGRAM + " LIKE ? AND " + MySQLiteHelper.UNIGRAM_INAPPROPRIATE + " == 0", new String[]{prefix+"%"},  // The values for the WHERE clause
                MySQLiteHelper.UNIGRAM_UNIGRAM + " >=  ? AND " + MySQLiteHelper.UNIGRAM_UNIGRAM + " <= ? AND " + MySQLiteHelper.UNIGRAM_INAPPROPRIATE + " == 0", new String[]{prefix, prefix+"ĐĐĐĐĐ"},
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder,
                limit                // The sort order
        );
        Unigram[] words = new Unigram[c.getCount()];
        int i = 0;
        c.moveToFirst();
        while (i < c.getCount()) {
            words[i] =  cursorToUnigram(c);
            i++;
            c.moveToNext();
        }
        //close cursor
        c.close();
        return words;
    }

    public int getUnigramId(String word){
        String[] projection = {
                MySQLiteHelper.UNIGRAM_ID
        };


        Cursor c = database.query(
                MySQLiteHelper.TABLE_UNIGRAMS,  // The table to query
                projection,                               // The columns to return
                // The columns for the WHERE clause
                MySQLiteHelper.UNIGRAM_UNIGRAM + " = ?", new String[]{word},  // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null
        );
        if (c.getCount() >= 1){
            c.moveToFirst();
            int id = c.getInt(0);
            c.close();
            return id;
        }
        else {
            c.close();
            return  -1;
        }
    }

    /**
     * Pretvara rezultat iz Cursora u Unigram objekt (pobrinuti se da Cursor ima sve potrebne stupce
     * U PRAVILNOM REDOSLIJEDU)
     *
     * @param cursor
     * @return
     */

    private Unigram cursorToUnigram(Cursor cursor) {
        Unigram unigram = new Unigram(cursor.getString(1), cursor.getInt(2));
        unigram.set_id(cursor.getInt(0));
        unigram.setInappropriate(cursor.getInt(3));
        unigram.setType(cursor.getInt(4));
        unigram.setRoot(cursor.getString(5));
        return unigram;
    }

    public void fillUnigramDatabaseFromFile(){
        new InitialLoadDbTask(mContext, database).execute();
    }

    public int countEntries() {
        String countQuery = "SELECT  * FROM " + MySQLiteHelper.TABLE_UNIGRAMS;
        Cursor cursor = database.rawQuery(countQuery, null);
        int cnt = cursor.getCount();
        cursor.close();
        return cnt;
    }

    @Override
    public int getFrequency(CharSequence word) {
        String[] projection = {
                MySQLiteHelper.UNIGRAM_FREQUENCY
        };
        Cursor cursor = database.query(
                MySQLiteHelper.TABLE_UNIGRAMS,
                projection,
                MySQLiteHelper.UNIGRAM_UNIGRAM + " = ?",
                new String[]{word.toString()},
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null
        );
        int freq = -1;
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            freq = cursor.getInt(0);
        }
        cursor.close();
        return freq;
    }

    /**
     * Vraca sve rijeci iz baze koje su duljine izmedju min i max
     *
     * @param min
     * @param max
     * @return
     */
    public Unigram[] getWordsBoundedByLength(int min, int max) {

        String[] projection = {
                MySQLiteHelper.UNIGRAM_ID,
                MySQLiteHelper.UNIGRAM_UNIGRAM,
                MySQLiteHelper.UNIGRAM_FREQUENCY,
                MySQLiteHelper.UNIGRAM_INAPPROPRIATE,
                MySQLiteHelper.UNIGRAM_TYPE,
                MySQLiteHelper.UNIGRAM_ROOT
        };
        Cursor cursor = database.query(
                MySQLiteHelper.TABLE_UNIGRAMS,
                projection,
                MySQLiteHelper.UNIGRAM_LENGTH + " > ? AND " + MySQLiteHelper.UNIGRAM_LENGTH + " < ?" ,
                new String[]{String.valueOf(min),String.valueOf(max)},
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null
        );
        int i = 0;
        if (cursor.getCount() > 0) {
            Unigram[] results = new Unigram[cursor.getCount()];
            cursor.moveToFirst();
            while (i < cursor.getCount()) {
                results[i] =  cursorToUnigram(cursor);
                i++;
                cursor.moveToNext();
            }
            cursor.close();
            return results;
        }
        else {
            cursor.close();
            return null;
        }
    }

    /**
     * Vraća unigram iz baze na temelju dane riječi. Case insensitive (ukoliko nije
     * promijenjena definicija baze).
     *
     * Ukoliko nije nađen vraća null.
     *
     * @param word
     * @return
     */
    public Unigram getUnigram(String word){
        Cursor c = database.query(
                MySQLiteHelper.TABLE_UNIGRAMS,  // The table to query
                allColumns,                               // The columns to return
                // The columns for the WHERE clause
                MySQLiteHelper.UNIGRAM_UNIGRAM + " = ?", new String[]{word},  // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null
        );
        if (c.getCount() >= 1){
            c.moveToFirst();
            Unigram ug = cursorToUnigram(c);
            return ug;
        }
        else {
            c.close();
            return null;
        }
    }


    /**
     * Async task to initially load the unigram database from text file, for testing mainly
     */
    private  class InitialLoadDbTask extends AsyncTask<Void, Void, Void> {

        private Context mContext;
        private SQLiteDatabase database;
        private int INPUT_MAXIMUM_FREQUENCY;

        public InitialLoadDbTask(Context context, SQLiteDatabase db) {
            mContext = context;
            database = db;
        }

        @Override
        protected Void doInBackground(Void... v) {
            if (USE_PRELOADED_DATABASE)
                return null;
            //todo remove ako nadjemo bolje rjesenje npr. SharedPreferences
            if (dbHelper.tableExists(MySQLiteHelper.TABLE_UNIGRAMS, database, false))
                return null;
            else {
                dbHelper.createUnigramTable(database);
            }
            AssetManager am = mContext.getAssets();
            InputStream inputStream;
            try {
                inputStream = am.open("testdict.txt");
            } catch (IOException e) {
                Log.e("SQLITE_TEST", "Testdicts not found!");
                return null;
            }
            BufferedReader br;
            //dodajmo iz datoteke
            try {
                br = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
                String line;
                while (database.inTransaction()){
                    try {
                        //wait for transaction to finish
                        wait(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                boolean read_max_frequency = false;
                database.beginTransaction();
                while ((line = br.readLine()) != null) {
                    //Log.d("SOFT_KEYBOARD", line);
                    String[] stuff = line.split(",");
                    if (!read_max_frequency){
                        INPUT_MAXIMUM_FREQUENCY = Integer.parseInt(stuff[3]);
                        read_max_frequency = true;
                    }
                    insertOrUpdateUnigram(new Unigram(stuff[0], normalizeFrequency(Integer.parseInt(stuff[3])), Integer.parseInt(stuff[4]),Integer.parseInt(stuff[5]), stuff[1]));
                }
                database.setTransactionSuccessful();
            } catch (UnsupportedEncodingException e) {
                Log.e("SQLITE_TEST", "Encoding not supported!");
                e.printStackTrace();
                return null;
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                database.endTransaction();
            }
            return null;
        }

        /**
         *** DEPRECATED !***
         *
         * Funkcija za normalizaciju frekvencija riječnika.
         * Rezultat normalkizacije ovisi, među ostalim i o maksimalnoj frekvenciji
         * koju je moguce pohraniti u rijecniku kako je definirano u SessionDictionary.
         * Algoritam normalizacije je sljedeci:
         *  - promatramo riječnik kao da jedan veliki dokument te se koristi
         *      maximum tf normalizacija, dakle ovisno o najčešćoj riječi u riječniku (koja mora biti 1.
         *      u ulaznom formatu)
         *  - dobivena relativna frekvencija se množi sa pola maksimalno moguće u riječniku pohrane
         *
         *  Ovime se dobiju najmanje početne frekvencije oko 6, što znači da će korisnik morati 6 puta
         *  iskoristiti neku svoju novu riječ da ona postane vrijedna kao i početno unesene riječi
         *
         *
         * @param freq
         * @return
         */
        private int normalizeFrequency(final int freq){
            float normalized = 0.05f + (0.95f * ((float) freq / (float)INPUT_MAXIMUM_FREQUENCY));
            return Math.round(normalized * (SessionDictionary.BIGRAM_MAX_FREQUENCY/2));
        }
    }
}
