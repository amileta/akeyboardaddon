package com.prediction;

/**
 * Created by Antonio on 4.6.2015..
 *
 * POJO klasa za riječ
 */
public class Unigram {

    public static int VRSTA_IMENICA = 1;
    public static int VRSTA_GLAGOL = 2;
    public static int VRSTA_PRIDJEV = 3;
    public static int VRSTA_ZAMJENICA = 4;
    public static int VRSTA_BROJ = 5;
    public static int VRSTA_PRILOG = 6;
    public static int VRSTA_PRIJEDLOG = 7;
    public static int VRSTA_VEZNIK = 8;
    public static int VRSTA_USKLIK = 9;
    public static int VRSTA_ČESTICA = 10;

    long _id;
    String word;
    String root;
    int frequency;
    int inappropriate;
    int type;

    private Unigram(){

    }

    public Unigram(String word, int frequency){
        this.word = word;
        this.frequency = frequency;
    }

    public Unigram(String word, int frequency, int inappropriate){
        this(word, frequency);
        this.inappropriate = inappropriate;
    }

    public Unigram(String word,
                   int frequency,
                   int inappropriate,
                   int type){
        this(word, frequency, inappropriate);
        this.type = type;
    }

    public Unigram(String word,
                   int frequency,
                   int inappropriate,
                   int type,
                   String root){
        this(word, frequency, inappropriate, type);
        this.root = root;
    }

    public Unigram(long _id, String word,
                   int frequency,
                   int inappropriate,
                   int type,
                   String root){
        this(word, frequency, inappropriate, type, root);
        this._id = _id;
    }

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public int isInappropriate() {
        return inappropriate;
    }

    public void setInappropriate(int inappropriate){
        if(inappropriate == 1){
            this.inappropriate = 1;
        }
        else {
            this.inappropriate = 0;
        }
    }

    public String getRoot() {
        return root;
    }

    public void setRoot(String root) {
        this.root = root;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }
}