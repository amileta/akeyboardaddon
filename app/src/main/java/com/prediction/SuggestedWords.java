package com.prediction;



import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Antonio on 16.5.2015..
 *
 * Klasa za pohranu predlozenih rijeci za tipkovnicu.
 */
public class SuggestedWords implements Parcelable {

    private ArrayList<String> suggestionWords;
    private ArrayList<Integer> suggestionScores;
    private int max_size;

    private SuggestedWords() {

    }

    public SuggestedWords(int size) {
        suggestionWords = new ArrayList<String>(size);
        suggestionScores = new ArrayList<Integer>(size);
        if (size <= 0) {
            throw new IllegalArgumentException("Velicina mora biti pozitivna!");
        }
        max_size = size;
    }

    public boolean contains(String word) {
        return suggestionWords.contains(word);
    }

    public boolean add(String word, int score) {
        //Log.d("SOFT_KEY", "Adding suggestion:" + word + "," + score);
        int i = 0;
        int c = suggestionWords.indexOf(word);
        //ako postoji rijec vec u prijedlozima, obrisi je ako ima manji score
        if (c != -1) {
            if (suggestionScores.get(c) < score) {
                suggestionWords.remove(c);
                suggestionScores.remove(c);
            }
            else {
                //inace je ignoriraj
                return true;
            }
        }
        //dodaj novu rijec u poredak, <= da stavi na kraj
        while (i < suggestionScores.size() && i < max_size && score <= suggestionScores.get(i)) i++;
        if (i != max_size) {
            suggestionScores.add(i, score);
            suggestionWords.add(i, word);
            //Log.d("SOFT_KEY", "SuggestedWords:" + this.toString());
            if (suggestionWords.size()-1 >= max_size) {
                suggestionScores.remove(max_size);
                suggestionWords.remove(max_size);
            }
            return true;
        }
        return false;
    }

    public int getScore(String word) {
        int i = suggestionWords.indexOf(word);
        if (i != -1)
            return suggestionScores.get(i);
        else
            return i;
    }

    public Integer remove(String word) {
        int i = suggestionWords.indexOf(word);
        if (i != -1) {
            suggestionWords.remove(i);
            return suggestionScores.remove(i);
        }
        return -1;
    }

    public void clear(){
        suggestionScores.clear();
        suggestionWords.clear();
    }

    public List<String> getSorted(){
        return getSorted(true);
    }

    public List<String> getSorted(final boolean descending) {
        if (descending) {
            return  (ArrayList<String>) suggestionWords.clone();
        } else {
            List<String> words = (ArrayList<String>) suggestionWords.clone();
            Collections.reverse(words);
            return words;
        }
    }

    @Override
    public String toString() {
        String out = new String();
        for (int i = 0; i < suggestionWords.size(); i++) {
            out += suggestionWords.get(i) + "," + suggestionScores.get(i).toString() + "\n";
        }
        return out;
    }

    public int size(){
        return suggestionWords.size();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringList(this.suggestionWords);
        dest.writeList(this.suggestionScores);
        dest.writeInt(this.max_size);
    }

    protected SuggestedWords(Parcel in) {
        this.suggestionWords = in.createStringArrayList();
        this.suggestionScores = new ArrayList<Integer>();
        in.readList(this.suggestionScores, List.class.getClassLoader());
        this.max_size = in.readInt();
    }

    public static final Parcelable.Creator<SuggestedWords> CREATOR = new Parcelable.Creator<SuggestedWords>() {
        public SuggestedWords createFromParcel(Parcel source) {
            return new SuggestedWords(source);
        }

        public SuggestedWords[] newArray(int size) {
            return new SuggestedWords[size];
        }
    };
}
