package com.prediction;

import android.content.Context;
import android.os.AsyncTask;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */


/**
 * Created by Antonio on 13.5.2015..
 *
 * Rijecnik za frekvencije unigrama (zasebnih rijeci)
 *
 *
 * This class (inherited from the old AutoDictionary) is used for user history
 * based dictionary. It stores words that the user typed to supply a provision
 * for suggesting and re-ordering of candidates.
 */
public class SessionUnigramDictionary extends SessionDictionary implements Dictionary.WordCallback {


    static final boolean ENABLE_USER_UNIGRAM_DICTIONARY = true;

    /**
     * Words that appear in both bigram and unigram data gets multiplier ranging from
     * BIGRAM_MULTIPLIER_MIN to BIGRAM_MULTIPLIER_MAX depending on the score from
     * bigram data.
     */
    public static final double BIGRAM_MULTIPLIER_MIN = 1.2;
    public static final double BIGRAM_MULTIPLIER_MAX = 1.5;
    /**
     * Maximum possible bigram frequency. Will depend on how many bits are being used in data
     * structure. Maximum bigram frequency will get the BIGRAM_MULTIPLIER_MAX as the multiplier.
     */
    public static final int MAXIMUM_BIGRAM_FREQUENCY = 127;

    public AsyncTask writeTask;

    private Context mContext;
    private int mDicTypeId;
    private int mMaxDepth;

    // Weight added to a user picking a new_prediction word from the suggestion strip
    static final int FREQUENCY_FOR_PICKED = 3;
    // Weight added to a user typing a new_prediction word that doesn't get corrected (or is reverted)
    static final int FREQUENCY_FOR_TYPED = 1;
    // If the user touches a typed word 2 times or more, it will become valid.
    //TODO promijenit u nesto smisleno
    private static final int VALIDITY_THRESHOLD = 2 * FREQUENCY_FOR_PICKED;

    // Locale for which this user unigram dictionary is storing words, unused
    private String mLocale;
    //todo promijenit da se pohranjuju unigram objekti da se ne izgube korijeni
    private HashMap<String,Integer> mPendingWrites = new HashMap<String,Integer>();
    private final Object mPendingWritesLock = new Object();

    private UnigramDataSource mUnigramDataSource;
    private SuggestedWords mSuggestedWords;

    public SessionUnigramDictionary(Context context, UnigramDataSource unigrams, String locale, int dicTypeId, SuggestedWords suggestedWords) {
        super(context, dicTypeId);
        if (!ENABLE_USER_UNIGRAM_DICTIONARY) return;
        mContext = context;
        mLocale = locale;
        mUnigramDataSource = unigrams;
        mSuggestedWords = suggestedWords;

        /*
        if (mLocale != null && mLocale.length() > 1) {
            loadDictionary();
        }*/
    }

    /**
     * Konstruktor za testiranje, ne koristi stvarnu tipkovnicu ni bazu
     * @param context
     * @param dicTypeId
     */
    public SessionUnigramDictionary(Context context, int dicTypeId, SuggestedWords suggestedWords) {
        super(context, dicTypeId);
        if (!ENABLE_USER_UNIGRAM_DICTIONARY) return;
        mContext = context;
        mLocale = "us_US";
        mSuggestedWords = suggestedWords;
        fillDictionaryForTesting();
        //blockingReloadDictionaryIfRequired();
    }

    /**
     * Vraća frekvenciju riječi u riječniku ukoliko je ona valjana (nalazi se u riječniku)
     * ili -1 ukoliko nije valjana. Gleda i u trenutni (session) dictionary i u underlying
     * database dictionary.
     *
     * @param word
     * @return
     */
    @Override
    public synchronized int isValidWord(String word) {
        if (!ENABLE_USER_UNIGRAM_DICTIONARY) return -1;
        int frequency = getWordFrequency(word);
        int dict_freq = mUnigramDataSource.getFrequency(word);
        return (frequency > dict_freq)  ? frequency : dict_freq;
    }

    @Override
    public void close() {
        super.close();
        if (!ENABLE_USER_UNIGRAM_DICTIONARY) return;
        //flushPendingWrites(); iskljuceno, posto pohranjujemo direktno u bazu
        // Don't close the database as locale changes will require it to be reopened anyway
        // Also, the database is written to somewhat frequently, so it needs to be kept alive
        // throughout the life of the process.
        // mOpenHelper.close();
    }

    public int getMaxWordLength() {
        return MAX_WORD_LENGTH;
    }

    /**
     * dodaje frekvenciju rijec u rijecnik i u queue za upis u bazu
     *
     *
     * @param newWord
     * @param addFrequency
     */
    public void addWord(String newWord, int addFrequency) {
        if (!ENABLE_USER_UNIGRAM_DICTIONARY) return;
        String word = newWord;
        final int length = word.length();
        // Don't add very short or very long words.
        if (length < 2 || length > getMaxWordLength()) return;
        // Remove caps before adding
        word = Character.toLowerCase(word.charAt(0)) + word.substring(1);
        int freq = getWordFrequency(word);
        freq = freq < 0 ? addFrequency : freq + addFrequency;
        super.addWord(word, freq);
    }

    public void getSuggestions(List<int[]> codes){
        getWords(codes, this, null);
    }

    /**
     * Schedules a background thread to write any pending words to the database.
     */
    public void flushPendingWrites() {
        if (!ENABLE_USER_UNIGRAM_DICTIONARY) return;
        synchronized (mPendingWritesLock) {
            // Nothing pending? Return
            if (mPendingWrites.isEmpty()) return;
            // Create a background thread to write the pending entries
            writeTask = new UpdateDbTask(mContext, mUnigramDataSource, mPendingWrites, mLocale).execute();
            // Create a new_prediction map for writing new_prediction entries into while the old one is written to db
            mPendingWrites = new HashMap<String, Integer>();
        }

    }

    /**
     * Async task to write pending words to the database so that it stays in sync with
     * the in-memory trie.
     */
    private static class UpdateDbTask extends AsyncTask<Void, Void, Void> {
        private final HashMap<String, Integer> mMap;
        private final UnigramDataSource mDataSource;
        private final String mLocale;
        private int errors = 0;

        public UpdateDbTask(@SuppressWarnings("unused") Context context, UnigramDataSource us, HashMap<String, Integer> pendingWrites, String locale) {
            mDataSource = us;
            mMap = pendingWrites;
            mLocale = locale;
        }
        @Override
        protected Void doInBackground(Void... v) {
            mDataSource.beginBulkOperation();
            for (Map.Entry<String, Integer> word : mMap.entrySet()) {
                //todo skuzit zasto ovdje dodje do nulla uopce
                if (word.getKey() == null) {
                    continue;
                }
                Unigram inserted = mDataSource.insertOrUpdateUnigram(new Unigram(word.getKey(), word.getValue()));
                if (inserted == null)
                    errors++;
            }
            if (errors == 0) {
                mDataSource.endBulkOperation(true);
            } else {
                mDataSource.endBulkOperation(false);
            }
            mMap.clear();
            return null;
        }
    }

    @Override
    public boolean addWord(final char[] word, int wordOffset, int wordLength, int score, int dicTypeId, Dictionary.DataType dataType) {
        String wordToAdd = new String(word,wordOffset,wordLength);
        //ako rijec vec postoji (dodali smo je iz podataka o bigramima), a sada dodajemo iz cached unigrama
        int oldscore = mSuggestedWords.getScore(wordToAdd);
        if (oldscore != -1){
            //ovdje praktički interpoliramo frekvenciju iz rječnika sa frekvencijom iz cache-a koja pamti broj
            //pojava u trenutnoj sesiji
            score = oldscore + score / mEntryCount;
        }
        //ako je nismo prije našli u rječniku iz nekog razloga ostavljamo trenutnu frekvenciju iz cache-a
        mSuggestedWords.add(wordToAdd, score);
        return true;
    }
}


