package com.prediction;

import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by Antonio on 4.6.2015..
 *
 * Obratiti pažnju na korištenje Singleton patterna kako bi mogli sigurno koristiti bazu u više
 * mjesta/threadova u aplikaciji.
 *
 * Modificirano tako da ubacuje/kopira preloaded bazu;
 *
 */
public class MySQLiteHelper extends SQLiteOpenHelper {

    private static MySQLiteHelper sInstance = null;

    //database version
    private static final int DATABASE_VERSION = 5;
    //database name
    private static String DATABASE_NAME = "akeyboard_prediction.db";

    //db paths todo find a way to make it a generic setting not hardcoded
    public static String DB_DIR = "/data/data/com.example.android.softkeyboard/databases/";
    private static String DB_PATH = DB_DIR + DATABASE_NAME;
    private static String OLD_DB_PATH = DB_DIR + "old_" + DATABASE_NAME;

    public static final String TABLE_UNIGRAMS = "unigram";
    public static final String UNIGRAM_ID = "_id";
    public static final String UNIGRAM_UNIGRAM = "unigram";
    public static final String UNIGRAM_ROOT = "root";
    public static final String UNIGRAM_TYPE = "type";
    public static final String UNIGRAM_INAPPROPRIATE = "inappropriate";
    public static final String UNIGRAM_FREQUENCY = "frequency";
    public static final String UNIGRAM_DEFAULT_FREQUENCY = "default_frequency";
    public static final String UNIGRAM_LENGTH = "length";

    public static final String UNIGRAM_INDEX_UNIGRAM = "unigram_idx";

    public static final String TABLE_BIGRAMS = "bigram";
    public static final String BIGRAM_ID = "_id";
    public static final String BIGRAM_FIRST_ID = "first_id";
    public static final String BIGRAM_SECOND_ID = "second_id";
    public static final String BIGRAM_FREQUENCY = "frequency";
    public static final String BIGRAM_DEFAULT_FREQUENCY = "default_frequency";

    //table creation sql statements
    // unigram table create statement
    private static final String CREATE_TABLE_UNIGRAM = "CREATE TABLE IF NOT EXISTS "
            + TABLE_UNIGRAMS + "(" + UNIGRAM_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + UNIGRAM_UNIGRAM + " STRING COLLATE NOCASE," + UNIGRAM_FREQUENCY + " INTEGER,"
            + UNIGRAM_INAPPROPRIATE + " INTEGER," + UNIGRAM_TYPE + " INTEGER,"
            + UNIGRAM_ROOT + " STRING," + UNIGRAM_DEFAULT_FREQUENCY + " INTEGER,"
            + UNIGRAM_LENGTH + " INTEGER, " + "UNIQUE(" +  UNIGRAM_UNIGRAM
            + "," + UNIGRAM_ROOT + "," + UNIGRAM_TYPE + ")"
            + ")";

    private static final String INDEX_TABLE_UNIGRAM = "CREATE INDEX " + UNIGRAM_INDEX_UNIGRAM + " ON "
            + TABLE_UNIGRAMS + "("+UNIGRAM_UNIGRAM+" COLLATE NOCASE )";

    // bigram table create statement
    private static final String CREATE_TABLE_BIGRAM = "CREATE TABLE IF NOT EXISTS " + TABLE_BIGRAMS
            + "(" + BIGRAM_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + BIGRAM_FIRST_ID + " INTEGER, "
            + BIGRAM_SECOND_ID + " INTEGER," + BIGRAM_FREQUENCY + " INTEGER," +  BIGRAM_DEFAULT_FREQUENCY + " INTEGER," + " FOREIGN KEY("
            + BIGRAM_FIRST_ID + ") REFERENCES " + TABLE_UNIGRAMS + "(" + UNIGRAM_ID + ") ON DELETE CASCADE,"
            + "FOREIGN KEY (" + BIGRAM_SECOND_ID + ") REFERENCES " + TABLE_UNIGRAMS + "(" + UNIGRAM_ID
            + ") ON DELETE CASCADE )";

    // flags for controlling creation or upgrading of database
    private static boolean mCreateDatabase = false;
    private static boolean mUpgradeDatabase = false;
    private Context mContext;

    public static synchronized MySQLiteHelper getInstance(Context context){
        if(sInstance == null){
            sInstance = new MySQLiteHelper(context);
        }
        return sInstance;
    }

    private MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.mContext = context;
        if (!databaseExists(context, DATABASE_NAME)){
            mCreateDatabase = true;
            initializeDatabase();
            Log.d("Database", "Database is not there: mUpgrade:" + mUpgradeDatabase + ";mCreate" + mCreateDatabase);
        } else {
            Log.d("Database", "Database exists!: mUpgrade:" + mUpgradeDatabase + ";mCreate" + mCreateDatabase);
        }
        DB_PATH = mContext.getDatabasePath(DATABASE_NAME).getAbsolutePath();
    }

    public void initializeDatabase() {
        /** Create a new database if it does not exist or upgrade an existing one **/

        getWritableDatabase();

        if (mCreateDatabase) {
            try {
                copyDatabase();
                Log.d("Database", "Database copied successfully!");
            } catch (IOException e) {
                Log.e("Database", "Error copying new database!");
                throw new Error("Error copying database");
            }
        } else if (mUpgradeDatabase) {
            try {
                FileHelper.copyFile(DB_PATH, OLD_DB_PATH);
                copyDatabase();
                SQLiteDatabase old_db = SQLiteDatabase.openDatabase(OLD_DB_PATH, null, SQLiteDatabase.OPEN_READWRITE);
                SQLiteDatabase new_db = SQLiteDatabase.openDatabase(DB_PATH, null, SQLiteDatabase.OPEN_READWRITE);
                /*
                 * todo Add code to load data into the new database from the old
                 * database and then delete the old database from internal
                 * storage after all data has been transferred.
                 *
                 */
            } catch (IOException e) {
                Log.e("Database", "Error copying upgraded database!");
                throw new Error("Error copying database");
            }
        }
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
    }

    public void dropTables(SQLiteDatabase database){
        Log.d("DBINFO", "Dropping tables..");
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_UNIGRAMS);
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_BIGRAMS);
    }

    public void deleteDatabase(){
        mContext.deleteDatabase(DATABASE_NAME);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        mUpgradeDatabase = true;
        initializeDatabase();
        //todo remove
        Log.w(MySQLiteHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data!");
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            // Enable foreign key constraints
            db.execSQL("PRAGMA foreign_keys=ON;");
        }
    }

    public void createUnigramTable(SQLiteDatabase database){
        database.execSQL(CREATE_TABLE_UNIGRAM);
        database.execSQL(INDEX_TABLE_UNIGRAM);
    }

    public void createBigramTable(SQLiteDatabase database){
        database.execSQL(CREATE_TABLE_BIGRAM);
    }

    /**
     * Provjerava ako tablica postoji u bazi.
     *
     * @param tableName
     * @param mDatabase
     * @param openDb
     * @return
     */

    public boolean tableExists(String tableName,SQLiteDatabase mDatabase, boolean openDb) {
        if(openDb) {
            if(mDatabase == null || !mDatabase.isOpen()) {
                mDatabase = getReadableDatabase();
            }

            if(!mDatabase.isReadOnly()) {
                mDatabase.close();
                mDatabase = getReadableDatabase();
            }
        }

        Cursor cursor = mDatabase.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '"+tableName+"'", null);
        if(cursor!=null) {
            if(cursor.getCount()>0) {
                cursor.close();
                return true;
            }
            cursor.close();
        }
        return false;
    }

    private static boolean databaseExists(Context context, String dbName) {
        File dbFile = context.getDatabasePath(dbName);
        return dbFile.exists();
    }

    /**
     *  Kopira preloaded bazu u novu bazu u direktoriju za baze.
     */
    public void copyDatabase() throws IOException {
        /* Close Helper so it commits the empty database to internal storage */
        close();

        /* Open database from assets */
        AssetManager assetManager = mContext.getResources().getAssets();
        InputStream in = null;
        OutputStream out = null;
        try {
            in = assetManager.open(DATABASE_NAME);
            out = new FileOutputStream(DB_PATH);
            FileHelper.copyFile(in, out);
        } catch (IOException e) {
            throw e;
        } finally {
            if (in != null) {
                in.close();
            }
            if (out != null) {
                out.close();
            }
        }
        /*Access copy so it is marked created */
        SQLiteDatabase db = getWritableDatabase();
        /*Upgrade version*/
        db.execSQL("PRAGMA user_version = " + DATABASE_VERSION);
        db.close();
        mCreateDatabase = false;
        mUpgradeDatabase = false;
    }

    public boolean isUpdating(){
        if (mUpgradeDatabase || mCreateDatabase)
            return true;
        return false;
    }

    public static void switchDatabase(String name){
        DATABASE_NAME = name;
        sInstance = null;
        Log.d("DATABASE","Switched database to: " + name);

    }
}
