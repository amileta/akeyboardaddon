package com.prediction;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.example.android.softkeyboard.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Antonio on 8.6.2015..
 */
public class PredictionService extends Service {

    /**
     * Command to the service to register a client, receiving callbacks
     * from the service.  The Message's replyTo field must be a Messenger of
     * the client where callbacks should be sent.
     */
    public static final int MSG_REGISTER_CLIENT = 1;

    /**
     * Command to the service to unregister a client, ot stop receiving callbacks
     * from the service.  The Message's replyTo field must be a Messenger of
     * the client as previously given with MSG_REGISTER_CLIENT.
     */
    public static final int MSG_UNREGISTER_CLIENT = 2;

    /**
     * Command to service to set a new value.  This can be sent to the
     * service to supply a new value to update the wanted prediction dictionary
     */
    public static final int MSG_SET_VALUE = 3;

    /**
     * Command to service to get the suggestions for this value.
     * Used in conjunction with PREDICTION_TYPE to choose the dictionary/es from which to pull
     * suggestions.
     */
    public static final int MSG_GET_VALUE = 4;
    public static final int MSG_SET_SUGGESTIONS = 5;

    /**
     * U ovom slucaju predati ce se predvidjena rijec i svi oblici te rijeci
     */
    public static final int MSG_SET_PREDICTED_FORMS = 6;

    /**
     * Prediction types the service provides
     */
    //the lowest level uses just the inbuilt default dictionary
    public static final int PREDICTION_TYPE_UNIGRAM = 0;
    public static final int PREDICTION_TYPE_BIGRAM = 1;
    //the user level uses the builtin and enables user-specific frequency tracking
    public static final int PREDICTION_TYPE_USER_UNIGRAM = 2;
    public static final int PREDICTION_TYPE_USER_BIGRAM = 3;
    //the session level uses the builtin, user-specific and temporary dictionary for the current
    //typing session
    public static final int PREDICTION_TYPE_SESSION_UNIGRAM = 4;
    public static final int PREDICTION_TYPE_SESSION_BIGRAM = 5;
    //use all available dictionaries
    public static final int PREDICTION_TYPE_ALL_UNIGRAM = 6;
    public static final int PREDICTION_TYPE_ALL_BIGRAM = 7;
    //use both unigram and bigram suggestions
    public static final int PREDICTION_TYPE_ALL = 8;
    //svi gramaticki oblici unigrama
    public static final int PREDICTION_TYPE_UNIGRAM_FORMS = 9;
    //get best predicted forms based on current bigram
    public static final int PREDICTION_TYPE_UNIGRAM_FORMS_BY_BIGRAM = 10;

    //multiplier za predikcije iz trenutne sesije
    public static final int USER_SESSION_MULTIPLIER = 5;

    public static final int MAX_WORD_LENGTH = 20;

    /** For showing and hiding our notification. */
    NotificationManager mNM;
    /** Keeps track of all current registered clients. */
    ArrayList<Messenger> mClients = new ArrayList<Messenger>();
    /** Holds last value set by a client. */
    int mValue = 0;

    private UnigramDataSource unigrams = null;
    private BigramDataSource bigrams = null;
    private SessionUnigramDictionary session_unigrams = null;
    private SessionBigramDictionary session_bigrams = null;

    private SuggestedWords mSuggestedWords = new SuggestedWords(16);
    private CorrectionDist mCorrectionDist;
    private Stemmer mStemmer;

    private boolean mAreDictionariesReady = false;

    /**
     * Handler of incoming messages from clients.
     */
    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            Bundle data;
            String word;
            String previous;
            switch (msg.what) {
                case MSG_REGISTER_CLIENT:
                    mClients.add(msg.replyTo);
                    break;
                case MSG_UNREGISTER_CLIENT:
                    mClients.remove(msg.replyTo);
                    break;
                case MSG_SET_VALUE:
                    data = msg.getData();
                    word = data.getString("word");
                    previous = data.getString("previous");
                    Log.d("SOFT_KEY", "Got word:" + word);
                    Log.d("SOFT_KEY", "Got previous:" + previous);
                    mValue = msg.arg1;
                    Log.d("SOFT_KEY", "Got value:" + mValue);
                    updateDictionary(word, previous, mValue);
                    break;
                case MSG_GET_VALUE:
                    data = msg.getData();
                    word = data.getString("word");
                    previous = data.getString("previous");
                    //Log.d("ASERVICE", "Got word:" + word);
                    mValue = msg.arg1;
                    //Log.d("ASERVICE", "Got value:" + mValue);
                    //Log.d("ASERVICE", "Got current:" + word);
                    //Log.d("ASERVICE", "Got previous:" + previous);
                    getSuggestions(word, previous, mValue);
                    try {
                        Message reply = fillMessageWithSuggestions(mValue);
                        msg.replyTo.send(reply);
                    } catch (RemoteException e) {
                        // The client is dead.  Remove it from the list;
                        mClients.remove(mClients.indexOf(msg.replyTo));
                    }
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }

    /**
     * Target we publish for clients to send messages to IncomingHandler.
     */
    final Messenger mMessenger = new Messenger(new IncomingHandler());

    @Override
    public void onCreate() {
        mNM = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);

        unigrams = new UnigramDataSource(this);
        bigrams = new BigramDataSource(this, unigrams);
        if (unigrams.open() && bigrams.open())
            mAreDictionariesReady = true;

        session_unigrams = new SessionUnigramDictionary(this.getApplicationContext(), unigrams, "hr_HR", Dictionary.DICT_TYPE_UNIGRAM, mSuggestedWords);
        session_bigrams = new SessionBigramDictionary(this.getApplicationContext(), bigrams, "hr_HR",Dictionary.DICT_TYPE_BIGRAM, mSuggestedWords );
        showNotification();
        mCorrectionDist = new CorrectionDist(mSuggestedWords);
        try {
            StemmerRulesProvider stemmerProvider = new StemmerRulesProvider(this.getApplicationContext());
            mStemmer = new Stemmer(stemmerProvider);
        } catch (IOException ex){
            Log.getStackTraceString(ex);
        }
    }

    @Override
    public void onDestroy() {
        // Cancel the persistent notification.
        mNM.cancel(R.string.remote_service_started);
        // Tell the user we stopped.
        /*Toast.makeText(this, R.string.remote_service_stopped, Toast.LENGTH_SHORT).show();*/
    }

    /**
     * When binding to the service, we return an interface to our messenger
     * for sending messages to the service.
     */
    @Override
    public IBinder onBind(Intent intent) {
        return mMessenger.getBinder();
    }

    /**
     * All clients have unbound with unbindService()
     * Stores prediction data for the current session and destroys the service
     *
     * @param intent
     * @return
     */
    @Override
    public boolean onUnbind(Intent intent) {
        //commented out because going for direct writing to database
        //storeSessionData();
        return false;
    }


    /**
     *
     * Generira povratnu poruku sa prijedlozima, te postavlja njenu vrijednost ovisno
     * o vrsti odgovora koja je potrebna kao zahtjev za predikciju
     *
     * @param predictionType
     * @return
     * @throws RemoteException
     */
    private Message fillMessageWithSuggestions (int predictionType) throws RemoteException {
        Message reply;
        if (predictionType == PREDICTION_TYPE_UNIGRAM_FORMS_BY_BIGRAM) {
             reply = Message.obtain(null,
                     MSG_SET_PREDICTED_FORMS, mValue, 0);
        } else {
            reply = Message.obtain(null,
                    MSG_SET_SUGGESTIONS, mValue, 0);
        }
        Bundle b = new Bundle();
        //Log.d("ASERVICE", "Suggestions:" + mSuggestedWords.toString());
        //return the requested suggestions
        b.putParcelable("suggestions", mSuggestedWords);
        reply.setData(b);
        return reply;
    }

    /**
     * Show a notification while this service is running.
     */
    private void showNotification() {
        // In this sample, we'll use the same text for the ticker and the expanded notification
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.icon_hr_hr)
                        .setContentTitle("Prediction Service")
                        .setContentText("Akeyboard prediction Service");
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        // mId allows you to update the notification later on.
        // We use a string id because it is a unique number.  We use it later to cancel.
        mNotificationManager.notify(R.string.remote_service_started, mBuilder.build());
    }

    /**
     *
     * Gets the suggestions depending on the type requested
     *
     * @param word word for suggestions
     * @param type suggestion type
     * @return
     */
    private SuggestedWords getSuggestions(String word, String previous, int type){

        if (!mAreDictionariesReady){
            mAreDictionariesReady = (unigrams.open() && bigrams.open());
            if (!mAreDictionariesReady)
                return null;
        }

        mSuggestedWords.clear();
        if (word.length() > MAX_WORD_LENGTH || (previous != null && previous.length() > MAX_WORD_LENGTH)){
            return null;
        }
        if (type == PREDICTION_TYPE_UNIGRAM){
            unigrams.getMatchingUnigramSuggestions(word, mSuggestedWords);
        }
        else if (type == PREDICTION_TYPE_BIGRAM){
            bigrams.getMatchingBigramSuggestions(previous, mSuggestedWords);
        }
        else if (type == PREDICTION_TYPE_SESSION_UNIGRAM || type == PREDICTION_TYPE_ALL_UNIGRAM){
            //todo jel mozemo ovo optimizirat?
            List<int[]> list = new ArrayList<int[]>();
            for (int i = 0; i < word.length(); i++) {
                list.add(new int[]{word.charAt(i)});
            }
            session_unigrams.getSuggestions(list);
            unigrams.getMatchingUnigramSuggestions(word, mSuggestedWords);
        }
        else if (type == PREDICTION_TYPE_ALL_BIGRAM || type == PREDICTION_TYPE_SESSION_BIGRAM){
            //ovo su user i system dict
            //ako je zapoceto pisanje trenutne rijeci, koristi filtriranje, inace nemoj
            if (word == null || word.isEmpty()) {
                bigrams.getMatchingBigramSuggestionsByProbabilities(previous, word, mSuggestedWords,false);
                //todo smislit da li koristiti bigrame iz sesije?
                session_bigrams.getSuggestionsFiltered(previous, word);
            } else {
                //bigrams.getMatchingBigramSuggestions(previous, word, mSuggestedWords, true);
                bigrams.getMatchingBigramSuggestionsByProbabilities(previous, word, mSuggestedWords, true);
                session_bigrams.getSuggestions(previous);
            }
        }
        //kombinacija unigrama i bigrama za bolje prijedloge
        else if (type == PREDICTION_TYPE_ALL){
            //todo jel mozemo ovo optimizirat?
            List<int[]> list = new ArrayList<int[]>();
            for (int i = 0; i < word.length(); i++) {
                list.add(new int[]{word.charAt(i)});
            }
            //najprije provjeri bigrame
            if (word != null &&  !word.isEmpty()) {
                bigrams.getMatchingBigramSuggestionsByProbabilities(previous, word, mSuggestedWords, true);
                session_bigrams.getSuggestionsFiltered(previous, word);

                //zatim provjeri unigrame
                unigrams.getMatchingUnigramSuggestions(word, mSuggestedWords, UnigramDataSource.INTERPOLATION_WEIGHT);
                session_unigrams.getSuggestions(list);
            }
        }
        //svi gramaticki oblici poslanog unigrama
        else if (type == PREDICTION_TYPE_UNIGRAM_FORMS){
            unigrams.getUnigramForms(word, mSuggestedWords);
        }
        //svi gramaticki oblici poredani prema frekvenciji bigrama kojeg bi tvorili
        else if (type == PREDICTION_TYPE_UNIGRAM_FORMS_BY_BIGRAM){
            unigrams.getUnigramFormsByBigram(previous, word, mSuggestedWords);
        }
        else {
            //if no matching prediction type found, return null.
            Log.e(this.getClass().getSimpleName(), "Requested prediction type not found found!");
            throw new IllegalArgumentException("Prediction type: " + type + " not found!");
        }
        //return
        return mSuggestedWords;
    }

    private boolean updateDictionary(String word, String previous, int type ){

        // don't try to store if db is not ready yet
        if (!mAreDictionariesReady){
            mAreDictionariesReady = (unigrams.open() && bigrams.open());
            if (!mAreDictionariesReady)
                return false;
        }

        //don't store long words
        if (word == null || word.length() > MAX_WORD_LENGTH || (previous != null && previous.length() > MAX_WORD_LENGTH)){
            return false;
        }
        if (type == PREDICTION_TYPE_USER_UNIGRAM){
            updateUserUnigramPrediction(word);
            return true;
        }
        else if (type == PREDICTION_TYPE_USER_BIGRAM) {
            if (previous == null)
                return false;
            updateUserBigramPrediction(word, previous);
            return true;
        }
        else if (type == PREDICTION_TYPE_SESSION_BIGRAM) {
            // write to session cache
            updateSessionBigramPrediction(word, previous);
            if (previous == null)
                return false;
            // and also to the underlying database
            updateUserBigramPrediction(word, previous);
            return true;
        }
        else if (type == PREDICTION_TYPE_SESSION_UNIGRAM) {
            updateSessionUnigramPrediction(word);
            updateUserUnigramPrediction(word);
            return true;
        }
        else if (type == PREDICTION_TYPE_ALL_BIGRAM){
            if (previous == null)
                return false;
            updateSessionBigramPrediction(word, previous);
            updateUserBigramPrediction(word, previous);
            return true;
        }
        else if (type == PREDICTION_TYPE_ALL_UNIGRAM){
            updateSessionUnigramPrediction(word);
            updateUserUnigramPrediction(word);
            return true;
        }
        return false;
    }

    /**
     * Update unigram prediction, using session cache
     *
     * @param word
     * @return
     */
    private boolean updateSessionUnigramPrediction(String word){
        //Log.d("ASERVICE", "Added unigram:" + word );
        session_unigrams.addWord(word, 1);
        return true;
    }

    /**
     * Update bigram prediction, using session cache
     *
     * @param word
     * @param previous
     * @return
     */
    private void updateSessionBigramPrediction(String word, String previous){
        if(previous != null && !previous.isEmpty()) {
            session_bigrams.addBigrams(previous, word);
        }
        else { //if previous is null, add unigram instead
            session_unigrams.addWord(word,1);
        }
    }

    /**
     * Update User Bigram prediction/suggestion, frequency included
     * @param word
     * @return
     */
    private void updateUserBigramPrediction(String word, String previous, int frequency){
        bigrams.insertOrUpdateBigram(new Unigram(previous,frequency),new Unigram(word, frequency), frequency);
    }

    /**
     * Update User Bigram prediction/suggestion
     * @param word
     * @return
     */
    private void updateUserBigramPrediction(String word, String previous){
        if (word == null || previous == null){
            throw new IllegalArgumentException("Current and previous words can't be null!");
        }

        Unigram ug1 = new Unigram(word, 1);
        //todo provjerit ako je ovo najbolje mjesto za ubacit Stemmera
        ug1.setRoot(mStemmer.korjenuj(word));
        Unigram ug2 = new Unigram(previous, 1);
        ug2.setRoot(mStemmer.korjenuj(previous));
        bigrams.insertOrUpdateBigram(ug2, ug1, 1);
    }

    /**
     * Update user Unigram prediction/suggestion
     * @param word
     * @return
     */
    private void updateUserUnigramPrediction(String word){
        if (word == null){
            throw new IllegalArgumentException("Word can't be null!");
        }
        Unigram unigram = new Unigram(word, 1);
        //todo provjerit ako je ovo najbolje mjesto za ukljucit stemmera
        unigram.setRoot(mStemmer.korjenuj(word.toLowerCase()));
        unigrams.insertOrUpdateUnigram(unigram);
    }

    /**
     * Dohvaća prijedloge na temelju Levenstein udaljenosti
     * Pretraga prijedloga je ograničena duljinom riječi na način da je minimalno
     * pola upisane, a maksimalno duljina upisane + 2
     */
    private void getSuggestionsBasedOnDistance(String word){
        Unigram[] wordsToSearch = unigrams.getWordsBoundedByLength(Math.max(word.length()/2, 1),word.length() + 2 );
        mCorrectionDist.setWords(wordsToSearch);
        mCorrectionDist.AddDistPredictions(word);
    }
}
