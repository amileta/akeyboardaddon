# README #

Ovaj repozitorij sadrži izvorni kod za Android aplikaciju Akeyboard (Assistive keyboard).
Aplikacija je izvedena kao Input Method kao te zamijenjuje nativnu tipkovnicu.
Ideja aplikacije je bila dodavanje i testiranje učinkovitosti unošenja teksta korištenjem skeniranja, 
funkcionalnosti koja je standardna u asistivnim tipkovnicama za druge uređaje.

Skeniranje je korisno kod osoba koje imaju smanjenu pokretljivost prstiju te ne mogu precizno utipkavati tekst na mobilnom uređaju.

### Features ###

* Standardna tipkovnica koja koristi custom layout
* Tipkovnica koja automatski skenira linearno
* Tipkovnica koja skenira metodom bisekcije
* Predikcija utipkanih riječi korištenjem ugrađenog riječnika i jednostavna metoda učenja često utipkavanih izraza